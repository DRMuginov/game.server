package game.server.utils

import java.util.*
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty1

inline fun <THIS, reified T> THIS.createOrAdd(listProperty: KMutableProperty1<THIS, List<T>?>, value: T) {
    var list = listProperty.get(this)
    if (list == null) {
        listProperty.set(this, listOf(value))
    } else {
        listProperty.set(this, list + value)
    }
}

inline fun <THIS, reified T> THIS.createOrAdd(listProperty: KMutableProperty1<THIS, List<T>?>, values: List<T>) {
    var list = listProperty.get(this)
    if (list == null) {
        listProperty.set(this, values.toList())
    } else {
        listProperty.set(this, list + values)
    }
}

inline fun <LIST : MutableList<T>, T> LIST.getOrAdd(predicate: (T) -> Boolean, newValue: () -> T): T {
    var value = this.firstOrNull(predicate)
    if (value == null) {
        value = newValue()
        this.add(value)
        return value
    }
    return value
}

inline fun <MAP : MutableMap<K, V>, reified K, reified V> MAP.getOrPut(key: K, newValue: () -> V): V {
    var value = this[key]
    if (value == null) {
        value = newValue()
        this[key] = value
        return value
    }
    return value
}

inline fun <THIS, reified MAP : MutableMap<K, V>, reified K, reified V> THIS.getOrCreateMapValue(mapProp: KMutableProperty1<THIS, MAP?>, newMap: () -> MAP, key: K, newValue: () -> V): V {
    return getOrCreateProp(mapProp, newMap).getOrPut(key, newValue)
}

inline fun <THIS, LIST : MutableList<V>, V> THIS.getOrCreateListValue(listProp: KMutableProperty1<THIS, LIST?>, newList: () -> LIST, predicate: (V) -> Boolean, newValue: () -> V): V {
    return getOrCreateProp(listProp, newList).getOrAdd(predicate, newValue)
}

inline fun <THIS, T> THIS.getOrCreateProp(prop: KMutableProperty1<THIS, T?>, newValue: () -> T): T {
    var value = prop.get(this)
    if (value == null) {
        value = newValue()
        prop.set(this, value)
        return value
    }
    return value
}

inline fun <SOURCE, T> setIfValueChanged(getSource: () -> SOURCE, prop: KMutableProperty1<SOURCE, T>, oldValue: T, newValue: T) {
    if (oldValue != newValue) {
        val source = getSource()
        prop.set(source, newValue)
    }
}

inline fun <T> forceForIndexed(list: List<T>, body: (T) -> Unit) {
    for (i in 0..list.lastIndex) {
        val value = list[i]
        body(value)
    }
}

inline fun <T> forceForIndexedReverted(list: List<T>, body: (T) -> Unit) {
    for (i in (list.lastIndex downTo 0)) {
        val value = list[i]
        body(value)
    }
}

inline fun <reified T> T.equalsBySingleField(other: Any?, prop: KProperty1<T, Any>): Boolean {
    if (this === other) return true
    if (other !is T) return false
    if (prop.get(this) != prop.get(other)) return false
    return true
}

inline fun <SOURCE, T> SOURCE.mergeOrSet(prop: KMutableProperty1<SOURCE, T?>, newValue: T, merge: (T, T) -> Unit) {
    val oldValue = prop.get(this)
    if (oldValue == null) {
        prop.set(this, newValue)
    } else {
        merge(oldValue, newValue)
    }
}

typealias Action1<T> = (T) -> Unit

data class ActionArray1<T>(
        val array: Array<Action1<T>>
) : Action1<T> {
    override fun invoke(t: T) {
        for (a in array) {
            a(t)
        }
    }

    companion object {
        private val EMPTY = ActionArray1<Any>(emptyArray())

        fun <T> empty(): ActionArray1<T> {
            return EMPTY as ActionArray1<T>
        }
    }
}

operator fun <T> Action1<T>?.minus(element: Action1<T>): Action1<T>? {
    when (this) {
        is ActionArray1 -> {
            val thisArray = this.array
            val index = thisArray.indexOfFirst { it == element }
            if (index == -1) return this
            return when (thisArray.size) {
                1 -> null
                2 -> this.array[1 - index]
                else -> ActionArray1(Arrays.copyOfRange(thisArray, 0, index) + Arrays.copyOfRange(thisArray, index + 1, thisArray.size))
            }
        }
        element -> return null
        else -> return this
    }
}

operator fun <T> Action1<T>?.plus(element: Action1<T>): Action1<T> {
    return when (this) {
        null -> element
        is ActionArray1 -> ActionArray1(this.array + element)
        else -> ActionArray1(arrayOf(this, element))
    }
}

inline fun <T> ActionArray1<T>.invokeLazy(lazy: () -> T) {
    if (this.array.isNotEmpty()) {
        val param = lazy()
        this.invoke(param)
    }

}