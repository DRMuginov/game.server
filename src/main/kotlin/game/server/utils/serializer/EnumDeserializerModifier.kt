package game.server.utils.serializer

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier

class EnumModifier {

    class Serializer : BeanSerializerModifier() {
        override fun modifyEnumSerializer(config: SerializationConfig, valueType: JavaType, beanDesc: BeanDescription, serializer: JsonSerializer<*>): JsonSerializer<*> {
            return object : JsonSerializer<Enum<*>>() {
                override fun serialize(value: Enum<*>?, gen: JsonGenerator, serializers: SerializerProvider) {
                    if (value == null) {
                        gen.writeNull()
                    } else {
                        gen.writeString(value.toString().toLowerCase())
                    }
                }
            }
        }

    }

    class Deserializer : BeanDeserializerModifier() {

        override fun modifyEnumDeserializer(config: DeserializationConfig?, type: JavaType, beanDesc: BeanDescription?, deserializer: JsonDeserializer<*>): JsonDeserializer<*> {
            return object : JsonDeserializer<Enum<*>>() {
                @Suppress("UNCHECKED_CAST")
                override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): Enum<*> {
                    try {
                        return getEnumValue(type.rawClass, jp.valueAsString.toUpperCase())
                    } catch (e: Throwable) {
                        return getEnumValue(type.rawClass, jp.valueAsString.toLowerCase())
                    }
                }
            }
        }
    }

    class KeySerializer : JsonSerializer<Enum<*>>() {
        override fun serialize(value: Enum<*>, gen: JsonGenerator, serializers: SerializerProvider) {
            gen.writeFieldName(value.toString().toLowerCase())
        }

    }

    open class EnumKeyDeserializer(val cls: Class<out Enum<*>>): KeyDeserializer() {
        override fun deserializeKey(key: String, ctxt: DeserializationContext): Any {
            try {
                return getEnumValue(cls, key.toUpperCase())
            } catch (e: Throwable) {
                return getEnumValue(cls, key.toLowerCase())
            }
        }

    }

    companion object {
        @Suppress("UNCHECKED_CAST")
        fun getEnumValue(enumClass: Class<*>, value: String): Enum<*> {
            val enumConstants = enumClass.enumConstants as Array<out Enum<*>>
            return enumConstants.first { it.name == value }
        }
    }
}

