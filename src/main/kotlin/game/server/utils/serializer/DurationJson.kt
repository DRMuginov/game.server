package game.server.utils.serializer

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import java.time.Duration

class DurationJson {

    class Serializer : JsonSerializer<Duration>() {
        override fun serialize(value: Duration, gen: JsonGenerator, serializers: SerializerProvider) {
            gen.writeNumber(value.toMillis())
        }
    }

    class Deserializer : JsonDeserializer<Duration>() {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Duration {
            return Duration.ofMillis(p.longValue)
        }
    }

}