package game.server.utils.jsondb

import org.hibernate.type.descriptor.WrapperOptions
import org.hibernate.type.descriptor.java.AbstractTypeDescriptor
import org.hibernate.type.descriptor.java.MutableMutabilityPlan
import org.hibernate.usertype.DynamicParameterizedType
import java.util.*

class JsonTypeDescriptor : AbstractTypeDescriptor<Any>(Any::class.java, object : MutableMutabilityPlan<Any>() {
    override fun deepCopyNotNull(value: Any): Any {
        return JacksonUtil.clone(value)
    }
}), DynamicParameterizedType {

    private var jsonObjectClass: Class<Any>? = null

    override fun setParameterValues(parameters: Properties) {
        jsonObjectClass = (parameters[DynamicParameterizedType.PARAMETER_TYPE] as DynamicParameterizedType.ParameterType)
                .returnedClass

    }

    override fun areEqual(one: Any?, another: Any?): Boolean {
        if (one === another) {
            return true
        }
        return if (one == null || another == null) {
            false
        } else JacksonUtil.toJsonNode(JacksonUtil.toString(one)).equals(
                JacksonUtil.toJsonNode(JacksonUtil.toString(another)))
    }

    override fun toString(value: Any?): String {
        return JacksonUtil.toString(value)
    }

    override fun fromString(string: String): Any {
        return JacksonUtil.fromString(string, jsonObjectClass)
    }

    @Suppress("UNCHECKED_CAST")
    override fun <X> unwrap(value: Any?, type: Class<X>, options: WrapperOptions): X? {
        if (value == null) {
            return null
        }
        if (String::class.java.isAssignableFrom(type)) {
            return toString(value) as X
        }
        if (Any::class.java.isAssignableFrom(type)) {
            return JacksonUtil.toJsonNode(toString(value)) as X
        }
        throw unknownUnwrap(type)
    }

    override fun <X> wrap(value: X?, options: WrapperOptions): Any? {
        return if (value == null) {
            null
        } else fromString(value.toString())
    }

}