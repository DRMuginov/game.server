package game.server.utils.jsondb

import org.hibernate.type.AbstractSingleColumnStandardBasicType
import org.hibernate.usertype.DynamicParameterizedType
import java.util.*

class JsonBinaryType : AbstractSingleColumnStandardBasicType<Any>(JsonBinarySqlTypeDescriptor.INSTANCE, JsonTypeDescriptor()), DynamicParameterizedType {

    override fun getName(): String {
        return TYPE
    }

    override fun setParameterValues(parameters: Properties) {
        (javaTypeDescriptor as JsonTypeDescriptor)
                .setParameterValues(parameters)
    }

    companion object {
        const val TYPE = "jsonb"
    }
}