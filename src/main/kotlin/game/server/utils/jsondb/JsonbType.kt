package game.server.utils.jsondb

import org.hibernate.HibernateException
import org.hibernate.engine.spi.SharedSessionContractImplementor
import org.hibernate.usertype.UserType

import java.io.Serializable
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

/**
 * Created by Miha on 14.03.2017.
 */
class JsonbType : UserType {
    override fun sqlTypes(): IntArray {
        return IntArray(0)
    }

    override fun returnedClass(): Class<*>? {
        return null
    }

    @Throws(HibernateException::class)
    override fun equals(x: Any, y: Any): Boolean {
        return false
    }

    @Throws(HibernateException::class)
    override fun hashCode(x: Any): Int {
        return 0
    }

    @Throws(HibernateException::class, SQLException::class)
    override fun nullSafeGet(rs: ResultSet, names: Array<String>, session: SharedSessionContractImplementor, owner: Any): Any? {
        return null
    }

    @Throws(HibernateException::class, SQLException::class)
    override fun nullSafeSet(st: PreparedStatement, value: Any, index: Int, session: SharedSessionContractImplementor) {

    }

    @Throws(HibernateException::class)
    override fun deepCopy(value: Any): Any? {
        return null
    }

    override fun isMutable(): Boolean {
        return false
    }

    @Throws(HibernateException::class)
    override fun disassemble(value: Any): Serializable? {
        return null
    }

    @Throws(HibernateException::class)
    override fun assemble(cached: Serializable, owner: Any): Any? {
        return null
    }

    @Throws(HibernateException::class)
    override fun replace(original: Any, target: Any, owner: Any): Any? {
        return null
    }
}
