package game.server.utils.jsondb

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper

import java.io.IOException

object JacksonUtil {

    val OBJECT_MAPPER = ObjectMapper()

    init {
        OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
    }

    fun <T: Any> fromString(string: String, clazz: Class<T>?): T {
        try {
            return OBJECT_MAPPER.readValue(string, clazz)
        } catch (e: IOException) {
            throw IllegalArgumentException("The given string value: "
                    + string + " cannot be transformed to Json object")
        }

    }

    fun toString(value: Any?): String {
        try {
            return OBJECT_MAPPER.writeValueAsString(value)
        } catch (e: JsonProcessingException) {
            throw IllegalArgumentException("The given Json object value: "
                    + value + " cannot be transformed to a String")
        }

    }

    fun toJsonNode(value: String?): JsonNode {
        try {
            return OBJECT_MAPPER.readTree(value)
        } catch (e: IOException) {
            throw IllegalArgumentException(e)
        }

    }

    fun <T: Any> clone(value: T): T {
        return fromString(toString(value), value.javaClass as Class<T>)
    }
}