package game.server.config

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import game.server.services.WebServer
import game.server.utils.serializer.DurationJson
import game.server.utils.serializer.EnumModifier
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement
import reactor.core.scheduler.Scheduler
import reactor.core.scheduler.Schedulers
import java.time.Duration
import java.util.concurrent.Executors

@ComponentScan("game.server")
@EntityScan("game.server.domain.entity")
@EnableJpaRepositories("game.server.domain.entity")
@EnableTransactionManagement
@EnableConfigurationProperties
@Configuration
class BaseConfig {

    /*@ConfigurationProperties("data-source")
    @Bean
    fun dataSource(): DataSource {
        return ComboPooledDataSource()
    }*/

    @Bean("workerScheduler")
    fun workerScheduler(): Scheduler {
        return Schedulers.fromExecutor(Executors.newFixedThreadPool(16))
    }

    @Bean("databaseScheduler")
    fun databaseScheduler(): Scheduler {
        return Schedulers.fromExecutor(Executors.newFixedThreadPool(16))
    }

    @Primary
    @Bean("jsonMapper")
    fun jsonMapper(): ObjectMapper {
        return configureObjectMapper(ObjectMapper())
    }

    @Bean("yamlMapper")
    fun yamlMapper(): YAMLMapper {
        val mapper = configureObjectMapper(YAMLMapper())
        mapper.enable(YAMLGenerator.Feature.MINIMIZE_QUOTES)
        mapper.disable(YAMLGenerator.Feature.USE_NATIVE_TYPE_ID)
        mapper.disable(YAMLGenerator.Feature.SPLIT_LINES)
        mapper.disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER)
        return mapper
    }

    fun <MAPPER : ObjectMapper> configureObjectMapper(objectMapper: MAPPER): MAPPER {
        objectMapper.registerKotlinModule()
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)

        val simpleModule = SimpleModule()

        simpleModule.addSerializer(Duration::class.java, DurationJson.Serializer())
        simpleModule.addDeserializer(Duration::class.java, DurationJson.Deserializer())

        simpleModule.setDeserializerModifier(EnumModifier.Deserializer())
        simpleModule.setSerializerModifier(EnumModifier.Serializer())
        objectMapper.registerModule(simpleModule)
        return objectMapper
    }

    @Bean
    @ConfigurationProperties("webserver.config")
    fun webServerConfig(): WebServer.WebServerConfig {
        return WebServer.WebServerConfig()
    }

    @Bean
    fun timer(): Scheduler {
        return Schedulers.newSingle("timer")
    }

    @Bean
    fun fileScheduler(): Scheduler {
        return Schedulers.newSingle("fileScheduler")
    }

    @Bean
    fun thirdPartyServiceScheduler(): Scheduler {
        return Schedulers.newSingle("thirdPartyServiceScheduler")
    }
}
