package game.server.config

import game.server.services.TimeService
import game.server.services.TimeServiceImpl
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(BaseConfig::class)
class AppConfig{

    @Bean
    fun timeService(): TimeService {
        return TimeServiceImpl()
    }
}