package game.server.domain.entity

import game.server.services.EntityDocument
import game.server.utils.jsondb.JsonBinaryType
import game.server.utils.jsondb.StringJsonUserType
import org.hibernate.annotations.Type
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.time.Instant
import javax.persistence.*

@Table(name = "account")
@Entity
data class AccountEntity(
        @SequenceGenerator(name = "account_seq_gen", sequenceName = "account_seq", initialValue = 1, allocationSize = 1)
        @GeneratedValue(generator = "account_seq_gen", strategy = GenerationType.SEQUENCE)
        @Id
        val id: Long = 0,
        @Column(updatable = false)
        override var created: Instant = Instant.MIN,
        override var modified: Instant = Instant.MIN,
        @Type(type = StringJsonUserType.STRING_TYPE)
        @Column(columnDefinition = JsonBinaryType.TYPE)
        override var json: String = "{}"
) : BaseJsonEntity<AccountDB>

interface AccountRepository : JpaRepository<AccountEntity, Long> {
    @Query("select p.id from AccountEntity p")
    fun getAllIds(): List<Long>
}

data class AccountDB(
        var exp: Long = 0,
        var points: Long = 0,
        var level: Int = 1
)

typealias AccountDocument = EntityDocument<AccountEntity, AccountDB>