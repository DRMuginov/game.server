package game.server.domain.entity

import game.server.utils.jsondb.JsonBinaryType
import game.server.utils.jsondb.StringJsonUserType
import org.hibernate.annotations.TypeDef
import org.hibernate.annotations.TypeDefs
import java.time.Instant
import javax.persistence.MappedSuperclass

@TypeDefs(
        TypeDef(name = JsonBinaryType.TYPE, typeClass = JsonBinaryType::class),
        TypeDef(name = StringJsonUserType.STRING_TYPE, typeClass = StringJsonUserType::class)
)
@MappedSuperclass
interface BaseJsonEntity<JSON> {
    var json: String
    var created: Instant
    var modified: Instant
}