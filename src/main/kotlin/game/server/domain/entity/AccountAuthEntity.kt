package game.server.domain.entity

import game.server.services.AccountAuthType
import game.server.utils.jsondb.JsonBinaryType
import game.server.utils.jsondb.StringJsonUserType
import org.hibernate.annotations.Type
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.time.Instant
import javax.persistence.*

@Table(name = "account_auth")
@Entity
data class AccountAuthEntity(
        @SequenceGenerator(name = "account_auth_seq_gen", sequenceName = "account_auth_seq", initialValue = 1, allocationSize = 1)
        @GeneratedValue(generator = "account_auth_seq_gen", strategy = GenerationType.SEQUENCE)
        @Id
        val id: Long = 0,
        var accountId: Long? = null,
        @Column(updatable = false)
        override var created: Instant = Instant.MIN,
        override var modified: Instant = Instant.MIN,
        @Enumerated(EnumType.STRING)
        val auth_type: AccountAuthType = AccountAuthType.login,
        @Type(type = StringJsonUserType.STRING_TYPE)
        @Column(columnDefinition = JsonBinaryType.TYPE)
        override var json: String = "{}"
) : BaseJsonEntity<Any>

interface AccountAuthRepository : JpaRepository<AccountAuthEntity, Long> {
    fun deleteByAccountId(accountId: Long)
        @Query("SELECT coalesce(max(ch.id), 0) FROM AccountAuthEntity ch")
        fun getMaxAccountId(): Long
}