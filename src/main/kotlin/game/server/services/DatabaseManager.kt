package game.server.services

import com.fasterxml.jackson.databind.ObjectMapper
import game.server.domain.entity.BaseJsonEntity
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Service
import java.time.Instant
import javax.persistence.EntityManager
import javax.persistence.Query
import javax.transaction.Transactional

@Transactional
@Service
class DatabaseManager(
        @Qualifier("jsonMapper")
        val jsonMapper: ObjectMapper,
        val timeService: TimeService,
        val entityManager: EntityManager
) {

    fun <T> execute(run: () -> T): T {
        return run()
    }

    fun <ENTITY : BaseJsonEntity<BASE>, ID, BASE, BODY : BASE> findById(jpaRepository: JpaRepository<ENTITY, ID>, id: ID, jsonClass: Class<BODY>): EntityDocument<ENTITY, BODY>? {
        val optionalEntity = jpaRepository.findById(id)
        if (!optionalEntity.isPresent) return null
        val entity = optionalEntity.get()
        val json = jsonMapper.readValue<BODY>(entity.json, jsonClass)
        return EntityDocument(entity, json)
    }

    fun <ENTITY : BaseJsonEntity<*>, ID> findById(jpaRepository: JpaRepository<ENTITY, ID>, id: ID): ENTITY? {
        val optionalEntity = jpaRepository.findById(id)
        if (!optionalEntity.isPresent) return null
        return optionalEntity.get()
    }

    fun <ENTITY : BaseJsonEntity<BASE>, ID, BASE, BODY : BASE> findAllById(jpaRepository: JpaRepository<ENTITY, ID>, ids: Collection<ID>, jsonClass: Class<BODY>): List<EntityDocument<ENTITY, BODY>> {
        val all = jpaRepository.findAllById(ids)
        return all.map { EntityDocument(it, jsonMapper.readValue(it.json, jsonClass)) }
    }

    fun <ENTITY : BaseJsonEntity<BASE>, BASE, BODY : BASE> findAll(jpaRepository: JpaRepository<ENTITY, *>, jsonClass: Class<BODY>): List<EntityDocument<ENTITY, BODY>> {
        val all = jpaRepository.findAll()
        return all.map { EntityDocument(it, jsonMapper.readValue(it.json, jsonClass)) }
    }

    fun <ENTITY : BaseJsonEntity<BASE>, BASE, BODY : BASE> prepareEntity(entity: ENTITY, json: BODY? = null) {
        if (entity.created == Instant.MIN) {
            entity.created = timeService.now()
        }
        entity.modified = timeService.now()
        if (json != null) {
            entity.json = jsonMapper.writeValueAsString(json)
        }
    }

    fun <ENTITY : BaseJsonEntity<BASE>, ID, BASE, BODY : BASE> save(jpaRepository: JpaRepository<ENTITY, ID>, entityDoc: EntityDocument<ENTITY, BODY>): ENTITY {
        return save(jpaRepository, entityDoc.entity, entityDoc.body)
    }

    fun <ENTITY : BaseJsonEntity<BASE>, ID, BASE, BODY : BASE> save(jpaRepository: JpaRepository<ENTITY, ID>, entity: ENTITY, json: BODY? = null): ENTITY {
        prepareEntity(entity, json)
        return jpaRepository.save(entity)
    }

    fun <ENTITY : BaseJsonEntity<BASE>, ID, BASE, BODY : BASE> saveAll(jpaRepository: JpaRepository<ENTITY, ID>, list: List<EntityDocument<ENTITY, BODY>>) {
        for (entityDocument in list) {
            prepareEntity(entityDocument.entity, entityDocument.body)
        }
        val entities = list.map{it.entity}
        jpaRepository.saveAll(entities)
    }

    @Suppress("UNCHECKED_CAST")
    fun <ENTITY : BaseJsonEntity<BASE>, BASE, BODY : BASE> nativeQuerySingle(sqlString: String, entityClass: Class<ENTITY>, jsonClass: Class<BODY>, setupQuery: (Query) -> Unit): EntityDocument<ENTITY, BODY>? {
        val query = entityManager.createNativeQuery(sqlString, entityClass)
        setupQuery(query)
        val entity = query.resultList.firstOrNull() as ENTITY? ?: return null
        val json = jsonMapper.readValue<BODY>(entity.json, jsonClass)
        return EntityDocument(entity, json)
    }

    @Suppress("UNCHECKED_CAST")
    fun <ENTITY : BaseJsonEntity<BASE>, BASE, BODY : BASE> nativeQueryListJson(sqlString: String, entityClass: Class<ENTITY>, jsonClass: Class<BODY>, setupQuery: (Query) -> Unit): List<BODY> {
        val query = entityManager.createNativeQuery(sqlString, entityClass)
        setupQuery(query)
        val entity = query.resultList as List<ENTITY>
        return entity.map { jsonMapper.readValue<BODY>(it.json, jsonClass) }
    }
}

data class EntityDocument<out ENTITY : BaseJsonEntity<*>, out BODY>(val entity: ENTITY, val body: BODY)