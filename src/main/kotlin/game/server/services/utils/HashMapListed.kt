package game.server.services.utils

import java.util.*

class HashMapListed<K, V>: MutableMap<K, V> {
    private val mapKeyIndex = HashMap<K, Int>()
    private val _keys = ArrayList<K>()
    private val _values = ArrayList<V>()

    override val size: Int
        get() = mapKeyIndex.size

    override fun containsKey(key: K): Boolean = mapKeyIndex.containsKey(key)

    override fun containsValue(value: V): Boolean = _values.contains(value)

    override fun get(key: K): V? {
        return mapKeyIndex[key]?.let { _values[it] }
    }

    override fun isEmpty(): Boolean = mapKeyIndex.isEmpty()

    override val entries: MutableSet<MutableMap.MutableEntry<K, V>>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
    override val keys: MutableSet<K>
        get()  = mapKeyIndex.keys
    override val values: MutableList<V> = _values

    override fun clear() {
        mapKeyIndex.clear()
        _keys.clear()
        _values.clear()
    }

    override fun put(key: K, value: V): V? {
        val index = mapKeyIndex[key]
        if (index != null) {
            return _values.set(index, value)
        } else {
            _keys.add(key)
            _values.add(value)
            mapKeyIndex[key] = _values.size-1
            return null
        }
    }

    override fun putAll(from: Map<out K, V>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun remove(key: K): V? {
        val removedIndex = mapKeyIndex.remove(key) ?: return null
        val lastIndex = _keys.lastIndex
        if (lastIndex != removedIndex) {
            mapKeyIndex[_keys[lastIndex]] = removedIndex
            Collections.swap(_keys, removedIndex, lastIndex)
            Collections.swap(_values, removedIndex, lastIndex)
        }
        _keys.removeAt(lastIndex)
        val removedValue = _values.removeAt(lastIndex)
        return removedValue
    }

    val valuesList: List<V> = _values
    val keysList: List<K> = _keys
}