package game.server.services.utils

import game.server.game.Account
import game.server.game.GameEvent
import game.server.game.api.utils.ApiMethod
import org.reactivestreams.Subscriber
import reactor.core.publisher.Mono
import java.util.concurrent.CompletableFuture

interface ChannelSession {
    var account: Account?
    val info: String
    fun send(event: GameEvent)
    fun close()
    fun onClose(): Mono<Unit>
    fun push()
    fun<PARAMS, RESULT> call(apiMethod: ApiMethod<PARAMS, RESULT>, params: PARAMS): Mono<RESULT>
}

open class ChannelSessionImpl(
        val subscriber: Subscriber<GameEvent>,
        override val info: String = "",
        val onPush: ()->Unit = {},
        val fromServerToClient: ApiMethodHandler = object: ApiMethodHandler{
            override fun <PARAMS, RESULT> handle(apiMethod: ApiMethod<PARAMS, RESULT>, params: PARAMS): Mono<RESULT> = return Mono.error(TODO())
        }
): ChannelSession {
    private val onClose: CompletableFuture<Unit> = CompletableFuture()

    override var account: Account? = null

    override fun send(event: GameEvent) {
        subscriber.onNext(event)
    }

    override fun<PARAMS, RESULT> call(apiMethod: ApiMethod<PARAMS, RESULT>, params: PARAMS): Mono<RESULT>{
        return fromServerToClient.handle(apiMethod, params)
    }

    override fun close() {
        onClose.complete(Unit)
    }

    override fun onClose(): Mono<Unit> {
        return Mono.fromFuture(onClose)
    }

    override fun push() {
        onPush()
    }
}

interface ApiMethodHandler {
    fun <PARAMS, RESULT> handle(apiMethod: ApiMethod<PARAMS, RESULT>, params: PARAMS): Mono<RESULT>
}