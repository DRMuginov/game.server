package game.server.services.utils

import game.server.game.api.GameApiEvents
import game.server.game.api.data.*
import game.server.game.api.utils.ApiEventData
import game.server.utils.createOrAdd
import game.server.utils.forceForIndexed
import game.server.utils.getOrCreateListValue
import game.server.utils.mergeOrSet

class ClientEventsStore(
        val send: (ApiEventData<*>) -> Unit
) {

    private val events: MutableList<ApiEventData<*>> = ArrayList()

    fun handleEvent(apiEventData: ApiEventData<*>) {
        when (apiEventData.apiEvent) {
            GameApiEvents.UPDATE_ACCOUNT -> addOrUpdate(apiEventData, { a, b -> a.accountId == b.accountId }, ::mergeAccountUpdate)
            else -> send(apiEventData)
        }
    }

    private fun mergeTimer(merge: TimerUpdateData, value: TimerUpdateData) {
        value.enabled?.let { merge.enabled = it }
        value.total?.let { merge.total = it }
        value.passed?.let { merge.passed = it }
    }

    private fun mergeAccountUpdate(merge: AccountUpdateData, value: AccountUpdateData) {
        value.level?.let { merge.level = it }
        value.exp?.let { merge.exp = it }
        value.points?.let { merge.points = it }
        value.expAdded?.let { merge.createOrAdd(AccountUpdateData::expAdded, it) }
        value.nextLevelExp?.let { merge.nextLevelExp = it }
        value.gameRoomId?.let { merge.gameRoomId = it }
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T> addOrUpdate(apiEventData: ApiEventData<*>, sameInstance: (T, T) -> Boolean, merge: (T, T) -> Unit) {

        val last = (events.lastIndex downTo 0)
                .map { events[it] }
                .filter { it.apiEvent == apiEventData.apiEvent }
                .filter { it.data?.javaClass == apiEventData.data?.javaClass }
                .firstOrNull()
        if (last != null && sameInstance(last.data as T, apiEventData.data as T)) {
            merge(last.data as T, apiEventData.data as T)
        } else {
            events.add(apiEventData)
        }
    }

    fun push() {
        forceForIndexed(events) { e ->
            send(e)
        }
        events.clear()
    }
}