package game.server.services

import game.server.game.Account
import game.server.game.AccountEvent
import game.server.services.utils.ChannelSession
import org.springframework.stereotype.Service

@Service
class GameSessionService{
    fun associate(session: ChannelSession, account: Account) {
        session.account = account
        addSession(session, account)
        val subscribe = account.eventPublisher().subscribe(session::send)
        session.onClose().subscribe {
            subscribe.dispose()
            removeSession(session, account)
        }
        session.send(AccountEvent.Login(account))
    }

    fun addSession(session: ChannelSession, account: Account) {
        synchronized(account.sessions) {
            account.sessions.add(session)
        }
    }

    fun removeSession(session: ChannelSession, account: Account) {
        synchronized(account.sessions) {
            account.sessions.remove(session)
        }
    }

}