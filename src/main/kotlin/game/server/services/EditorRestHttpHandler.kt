package game.server.services

import com.fasterxml.jackson.databind.ObjectMapper
import io.undertow.server.HttpServerExchange
import io.undertow.util.Headers
import game.server.game.api.EditorApi
import game.server.game.api.data.JsonError
import game.server.game.api.exceptions.JsonErrorException
import game.server.game.api.utils.ApiMethod
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import java.io.ByteArrayOutputStream
import java.io.InputStream


@Component("editorRestHttpHandler")
class EditorRestHttpHandler(
        @Qualifier("jsonMapper")
        val jsonMapper: ObjectMapper,
        val editorApiMethodHandler: ApiMethodHandler
): WebServer.RestHttpHandler {

    val log = KotlinLogging.logger {}
    val apiMethodsMap: Map<String, ApiMethod<*, *>> = EditorApi.values().associateBy({ it.name })

    @Suppress("UNCHECKED_CAST", "UNREACHABLE_CODE")
    override fun handleRequest(exchange: HttpServerExchange, method: String) {
        val apiMethod = apiMethodsMap[method]
        if (apiMethod == null) {
            exchange.statusCode = 400
            exchange.responseSender.send(jsonMapper.writeValueAsString(error(JsonError(message = "method not exists"))))
        } else {
            exchange.responseHeaders.put(Headers.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            try {
                val body = toString(exchange.inputStream)
                val params = jsonMapper.readValue(body, apiMethod.paramsType)!!
                val handle = editorApiMethodHandler.handle(apiMethod as ApiMethod<Any, Any>, params)
                val block = handle.block()
                exchange.responseSender.send(jsonMapper.writeValueAsString(block))
            } catch (e: Throwable) {
                exchange.statusCode = 400
                exchange.responseSender.send(jsonMapper.writeValueAsString(error(e)))
            }
        }
    }

    private fun error(e: Throwable): JsonError {
        return when(e) {
            is JsonErrorException -> {
                e.jsonError
            }
            else -> {
                JsonError(message = e.cause?.toString()
                        ?: "", details = e.message)
            }
        }
    }

    fun toString(inputStream: InputStream): String {
        val result = ByteArrayOutputStream()
        val buffer = ByteArray(1024)
        var length: Int = 0
        while (inputStream.read(buffer).let { length = it; length != -1 }) {
            result.write(buffer, 0, length)
        }
        return result.toString(Charsets.UTF_8.toString())
    }
}