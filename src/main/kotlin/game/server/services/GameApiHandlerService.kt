package game.server.services

import com.fasterxml.jackson.databind.JsonNode
import game.server.game.Account
import game.server.game.api.GameApi
//import game.server.game.api.data.GameGetFile
//import game.server.game.api.exceptions.JsonErrorException
import game.server.game.api.utils.ApiMethod
import game.server.services.utils.ChannelSession
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class GameApiHandlerService(
        val accountAuthService: AccountAuthService,
        val accountService: AccountService,
        val gameService: GameService,
        val gameSessionService: GameSessionService,
        val gameLoopService: GameLoopService
) : SessionApiMethodHandler<ChannelSession> {

    private val map = mutableMapOf<String, SessionMethodHandlerMono<*, *>>()

    @Suppress("UNCHECKED_CAST")
    override fun <PARAMS, RESULT> handle(apiMethod: ApiMethod<PARAMS, RESULT>, params: PARAMS, context: ChannelSession): Mono<RESULT> {
        val apiMethodHandler = map[apiMethod.name] as SessionMethodHandlerMono<PARAMS, RESULT>?
        return if (apiMethodHandler == null) {
            Mono.error(NotImplementedError("method ${apiMethod.name} not implemented"))
        } else {
            apiMethodHandler(context, params)
        }
    }

    init {
        add(GameApi.AUTH_REGISTER, { session, params -> authSession(session, accountAuthService.regLogin(params)) })
        add(GameApi.AUTH_LOGIN, { session, params -> authSession(session, accountAuthService.authLogin(params)) })

        add(GameApi.PLAYER_GET_INFO, requireAuth(gameService::getInfo))
        add(GameApi.PLAYER_ADD_EXP, requireAuth(gameService::addExp))
        add(GameApi.PLAYER_ADD_POINTS, requireAuth(gameService::addPoints))

        add(GameApi.GAME_GET_PLAYERS, requireAuth(gameService::getOnline))
        add(GameApi.GAME_START_GAME, requireAuth(gameService::startGame))
        add(GameApi.GAME_SEND_DATA, requireAuth(gameService::sendData))
        add(GameApi.GAME_END_GAME, requireAuth(gameService::endGame))
        //add(GameApi.GAME_GET_FILE, ::getFile)
    }

    /*private fun getFile(channelSession: ChannelSession, gameGetFile: GameGetFile): Mono<JsonNode> {
        return Mono.fromCallable {
            val node = configService.getConfigNode(gameGetFile.name) ?: NullNode.instance
            node
        }
    }*/

    private fun <PARAMS, RESULT> mono(next: (Account, ChannelSession, PARAMS) -> RESULT): (Account, ChannelSession, PARAMS) -> Mono<RESULT> {
        return { account, channel, params ->
            Mono.fromCallable {
                next(account, channel, params)
            }
        }
    }

    private fun <PARAMS, RESULT> requireAuth(next: (Account, ChannelSession, PARAMS) -> RESULT): SessionMethodHandlerMono<PARAMS, RESULT> =
            { session, params ->
                val account = session.account
                if (account == null) {
                    Mono.error(Exception("not authorized"))
                } else {
                    scheduleInGameLoop { next(account, session, params) }
                }
            }

    private fun <PARAMS, RESULT> requireAuthMono(next: (Account, ChannelSession, PARAMS) -> Mono<RESULT>): SessionMethodHandlerMono<PARAMS, RESULT> =
            { session, params ->
                val account = session.account
                if (account == null) {
                    Mono.error(Exception("not authorized"))
                } else {
                    next(account, session, params)
                }
            }

    private fun authSession(session: ChannelSession, monoAccount: Mono<Account>): Mono<Unit> {
        return monoAccount.map { gameSessionService.associate(session, it) }
    }

    final fun <PARAMS, RESULT> add(apiMethod: ApiMethod<PARAMS, RESULT>, handler: SessionMethodHandlerMono<PARAMS, RESULT>) {
        map[apiMethod.name] = handler
    }

    private fun <PARAMS, RESULT> schedule(function: SessionMethodHandler<PARAMS, RESULT>): SessionMethodHandlerMono<PARAMS, RESULT> {
        return { session, params ->
            scheduleInGameLoop {
                function(session, params)
            }
        }
    }

    private fun <T> scheduleInGameLoop(function: () -> T): Mono<T> {
        return gameLoopService.scheduleMono(function)
    }

}

interface SessionApiMethodHandler<in CONTEXT> {
    fun <PARAMS, RESULT> handle(apiMethod: ApiMethod<PARAMS, RESULT>, params: PARAMS, context: CONTEXT): Mono<RESULT>
}

typealias SessionMethodHandler<PARAMS, RESULT> = (ChannelSession, PARAMS) -> RESULT
typealias SessionMethodHandlerMono<PARAMS, RESULT> = SessionMethodHandler<PARAMS, Mono<RESULT>>