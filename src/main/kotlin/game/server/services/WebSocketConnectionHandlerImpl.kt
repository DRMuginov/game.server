package game.server.services

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import game.server.game.GameEvent
import io.undertow.websockets.core.WebSocketChannel
import io.undertow.websockets.core.WebSockets
import game.server.game.api.data.*
import game.server.game.api.utils.*
import game.server.game.api.GameApi
import game.server.services.utils.ChannelSession
import game.server.services.utils.ChannelSessionImpl
import game.server.services.utils.ClientEventsStore
import game.server.utils.forceForIndexed
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component
import reactor.core.publisher.DirectProcessor
import reactor.core.scheduler.Scheduler
import java.nio.ByteBuffer
import java.util.*
import java.util.function.Consumer

@Component
class WebSocketConnectionHandlerImpl(
        val sessionApiMethodHandler: SessionApiMethodHandler<ChannelSession>,
        @Qualifier("workerScheduler")
        val workerScheduler: Scheduler,
        val gameService: GameService,
        val jsonMapper: ObjectMapper
) : WebServer.WebSocketConnectionHandler {

    val log = KotlinLogging.logger {}
    val peers = ArrayList<WebSocketChannel>()
    val apiMethodsMap: Map<String, ApiMethod<*, *>> = GameApi.values().associateBy({ it.name })

    init {
        Thread(::pingLoop).apply {
            isDaemon = true
            name = "websocket_ping"
        }.start()
    }

    private fun pingLoop() {
        while (true){
            try {
                pingAll()
            } catch (e: Throwable) {
                log.error(e){"pingAll"}
            }
            Thread.sleep(5000)
        }
    }

    private fun pingAll() {
        synchronized(peers) {
            val data = ByteBuffer.allocate(0)
            forceForIndexed(peers) {peer->
                WebSockets.sendPing(data, peer, null)
            }
        }
    }

    override fun onConnect(channel: WebSocketChannel) {
        log.info { "websocket.connect: ${channel.peerAddress}" }
        synchronized(peers) { peers.add(channel) }
        channel.setAttribute(SESSION, createSession(channel))
    }

    override fun onMessage(channel: WebSocketChannel, message: String) {
        val session = channel.getAttribute(SESSION) as ChannelSession
        log.info { "websocket.textMessage:  peer = [${session.account?.accountId ?: -1},${channel.peerAddress}], message = $message" }
        try
        {
            val jsonMessage = jsonMapper.readValue<JsonMessage>(message)
            when (jsonMessage.type) {
                JsonMessageType.METHOD -> {
                    processMethod(session, channel, jsonMessage)
                    }
                else -> {
                    sendError(channel, jsonMessage.id, JsonError("bad message type: ${jsonMessage.type}"))
                }
            }
        } catch (e: Exception) {
            log.info { "Error onMessage" }
            log.info { e }
            sendError(channel, null, e)
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun processMethod(session: ChannelSession, channel: WebSocketChannel, jsonMessage: JsonMessage) {
        val id = jsonMessage.id
        val name = jsonMessage.name
        if (name != null) {
            val apiMethod = apiMethodsMap[name]
            if (apiMethod != null) {
                try {
                    val params = jsonMapper.treeToValue(jsonMessage.data, apiMethod.paramsType)!!
                    val handle = sessionApiMethodHandler.handle(apiMethod as ApiMethod<Any, Any>, params, session)
                    handle.publishOn(workerScheduler).subscribe(
                            Consumer { send(channel, JsonMessage(type = JsonMessageType.RESULT, id = id, data = jsonMapper.valueToTree(it))) },
                            Consumer { log.error(it) {};sendError(channel, id, it) }
                    )
                } catch (e: Throwable) {
                    sendError(channel, id, e)
                }
            } else {
                sendError(channel, id, JsonError(message = "method not found"))
            }
        } else {
            sendError(channel, id, JsonError(message = "method is null"))
        }
    }

    private fun sendError(channel: WebSocketChannel, id: String?, e: Throwable) {
        sendError(channel, id, JsonError(message = e.cause?.toString()
                        ?: "", details = e.message))
    }

    private fun sendError(channel: WebSocketChannel, id: String?, jsonError: JsonError) {
        send(channel, JsonMessage(type = JsonMessageType.ERROR, id = id, data = error(jsonError)))
    }

    private fun error(jsonError: JsonError): JsonNode {
        return jsonMapper.valueToTree(jsonError)
    }

    override fun onClose(channel: WebSocketChannel) {
        log.info { "websocket.close: ${channel.peerAddress}" }
        synchronized(peers) { peers.remove(channel) }
        val channelSession = channel.getAttribute(SESSION) as ChannelSession? ?: return
        channelSession.close()

    }

    private fun createSession(channel: WebSocketChannel): ChannelSession {
        val emitter = DirectProcessor.create<GameEvent>()
        val clientEventsStore = ClientEventsStore { sendEvent(channel, it) }
        val session = ChannelSessionImpl(subscriber = emitter, onPush = clientEventsStore::push)
        session.onClose().subscribe {
            //channel.sendClose()
        }
        emitter.subscribe { gameService.toApiEvent(it, clientEventsStore::handleEvent) }
        return session
    }

    private fun send(channel: WebSocketChannel, jsonMessage: JsonMessage) {
        val session = channel.getAttribute(SESSION) as ChannelSession
        val json = jsonMapper.writeValueAsString(jsonMessage)
        log.info { "websocket.sendText: peer = [${session.account?.accountId ?: -1},${channel.peerAddress}], message = $json" }
        WebSockets.sendText(json, channel, null)
    }

    private fun <T> sendEvent(channel: WebSocketChannel, apiEventData: ApiEventData<T>) {
        val jsonMessage = JsonMessage(type = JsonMessageType.EVENT, name = apiEventData.apiEvent.name, data = jsonMapper.valueToTree(apiEventData.data))
        return send(channel, jsonMessage)
    }

    companion object {
        const val SESSION = "SESSION"
    }
}

