package game.server.services

import mu.KotlinLogging
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

@Service
class StartupService(
        val gameLoopService: GameLoopService,
        val accountService: AccountService,
        val webServer: WebServer
) {

    val log = KotlinLogging.logger { }

    @PostConstruct
    fun main() {
        log.info { "start gameLoopService" }
        gameLoopService.start()
        attachToGameLoop()
        log.info { "start webServer" }
        webServer.start()
        registerShutdownHook()
    }

    private fun attachToGameLoop() {
        gameLoopService.tickListeners += accountService

    }

    private fun registerShutdownHook() {
//        Runtime.getRuntime().addShutdownHook(Thread {
//            shutdown()
//        })
    }

    @PreDestroy
    fun shutdown() {
        log.info { "shutdown begin" }
        log.info { "stopping webServer" }
        webServer.stop()
        log.info { "stopping gameLoopService" }
        gameLoopService.stop()
        log.info { "shutdown success" }
    }

}