package game.server.services

import com.fasterxml.jackson.databind.ObjectMapper
import io.undertow.Handlers
import io.undertow.Undertow
import io.undertow.server.handlers.ExceptionHandler
import io.undertow.util.Headers
import io.undertow.websockets.core.*
import io.undertow.Handlers.path
import io.undertow.attribute.ExchangeAttributes
import io.undertow.predicate.Predicates
import io.undertow.server.HttpServerExchange
import io.undertow.server.handlers.BlockingHandler
import io.undertow.util.PathTemplateMatch
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.MediaType
import org.springframework.stereotype.Service


@Service
class WebServer(
        val config: WebServerConfig,
        val webSocketConnectionHandler: WebSocketConnectionHandler,
        @Qualifier("editorRestHttpHandler")
        val httpRestHandler: RestHttpHandler
){

    val log = KotlinLogging.logger { }
    val objectMapper: ObjectMapper = ObjectMapper()
    var stopAction: () -> Unit = {}

    fun start() {
        val restHandler = Handlers.routing()
                .post(
                        "${config.apiPath}/{method}",
                        Predicates.contains(ExchangeAttributes.requestHeader(Headers.CONTENT_TYPE), MediaType.APPLICATION_JSON_VALUE),
                        BlockingHandler {
                            val pathMatch = it.getAttachment(PathTemplateMatch.ATTACHMENT_KEY)
                            val method = pathMatch.parameters["method"]!!

                            httpRestHandler.handleRequest(it, method)
                        })

        val websocketHandler = Handlers.path(restHandler)
                .addPrefixPath(config.wsPath, Handlers.websocket { _, channel ->
                    webSocketConnectionHandler.onConnect(channel)
                    channel.receiveSetter.set(object : AbstractReceiveListener() {
                        override fun onFullTextMessage(channel: WebSocketChannel, message: BufferedTextMessage) {
                            webSocketConnectionHandler.onMessage(channel, message.data)
                        }

                        override fun onClose(webSocketChannel: WebSocketChannel, channel: StreamSourceFrameChannel) {
                            super.onClose(webSocketChannel, channel)
                            webSocketConnectionHandler.onClose(webSocketChannel)
                        }

                        override fun onPing(webSocketChannel: WebSocketChannel?, channel: StreamSourceFrameChannel?) {
                            super.onPing(webSocketChannel, channel)
                        }
                    })
                    channel.resumeReceives()
                })

        val exceptionHandler = Handlers.exceptionHandler(websocketHandler)
                .addExceptionHandler(Exception::class.java) { exchange ->
                    val responseSender = exchange.responseSender
                    val ex = exchange.getAttachment(ExceptionHandler.THROWABLE)
                    exchange.responseHeaders.put(Headers.CONTENT_TYPE, "application/json")
                    responseSender.send(objectMapper.writeValueAsString(mapOf("exception" to ex.toString())))
                }

        val shutdownHandler = Handlers.gracefulShutdown(exceptionHandler)
        val server = Undertow.builder()
                .addHttpListener(config.port, config.host)
                .setHandler(shutdownHandler)
                .build()
        server.start()
        stopAction = {
            shutdownHandler.shutdown()
            shutdownHandler.awaitShutdown()
            server.stop()
        }


        for (listenerInfo in server.listenerInfo) {
            log.info { "server run at ${listenerInfo.address}" }
        }
    }

    fun stop() {
        stopAction()
    }

    interface WebSocketConnectionHandler {
        fun onConnect(channel: WebSocketChannel)
        fun onMessage(channel: WebSocketChannel, message: String)
        fun onClose(channel: WebSocketChannel)
    }

    data class WebServerConfig(
            var host: String = "localhost",
            var port: Int = 8081,
            var wsPath: String = "/ws",
            var apiPath: String = "/api"
    )

    interface RestHttpHandler {
        fun handleRequest(exchange: HttpServerExchange, method: String)
    }
}