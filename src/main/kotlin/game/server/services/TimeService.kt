package  game.server.services

import java.time.Instant

interface TimeService {
    fun now(): Instant
    fun currentTimeMillis(): Long
}