package  game.server.services

import java.time.Instant

class TimeServiceImpl: TimeService {
    override fun now(): Instant = Instant.now()

    override fun currentTimeMillis(): Long = System.currentTimeMillis()
}