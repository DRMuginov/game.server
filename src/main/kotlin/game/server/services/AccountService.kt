package game.server.services

import game.server.game.*
import game.server.game.api.data.AccountData
import game.server.game.api.data.AddData
import game.server.game.api.data.GameRoomId
import game.server.game.util.ITick
import game.server.game.util.TickContext
import game.server.services.utils.ChannelSession
import game.server.services.utils.HashMapListed
import game.server.utils.forceForIndexed
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import reactor.core.publisher.DirectProcessor
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.scheduler.Scheduler
import java.util.*
import java.util.concurrent.atomic.AtomicLong

@Service
class AccountService: ITick {

    val log = KotlinLogging.logger { }

    val cache = HashMapListed<Long, Account>()

    private val eventEmitter = DirectProcessor.create<Account>()

    val genId = AtomicLong(0)

    fun createAccount(): Account {
        return createAccount(genId())
    }

    private fun createAccount(accountId: Long): Account {
        val account = Account().apply {
            this.accountId = accountId
        }
        register(account)
        return account
    }

    fun genId(): Long {
        return genId.incrementAndGet()
    }

    fun register(account: Account) {
        cache[account.accountId] = account
        eventEmitter.onNext(account)
    }

    fun onAccountInit(): Flux<Account> {
        return eventEmitter
    }

    fun getAccount(accountId: Long): Account? {
        val cachedAccount = cache[accountId]
        if (cachedAccount != null) return cachedAccount
        return null
    }

    fun getPlayers(gameRoom: GameRoom): List<Account?> {
        val cachedAccount1 = cache[gameRoom.accountId1]
        val cachedAccount2 = cache[gameRoom.accountId2]
        return listOf(cachedAccount1, cachedAccount2)
    }

    fun getAllAccounts(): List<Account> = cache.valuesList

    fun updateExp(account: Account, update: AddData) {
        update.count?.let {
            addExp(account, it)
        }
    }

    fun updatePoints(account: Account, update: AddData) {
        update.count?.let {
            addPoints(account, it)
        }
    }

    fun setName(account: Account, name: String) {
        account.name = name
    }

    fun addPoints(account: Account, count: Long) {
        account.pointsAdded(count)
        account.points = count
    }

    fun addExp(account: Account, count: Long) {
        account.expAdded(count)
        var newExp = account.exp + count
        var nextLevelExp = account.nextLevelExp
        var level = account.level
        while (newExp > nextLevelExp) {
            newExp -= nextLevelExp
            level++
            nextLevelExp = getExpForNextLevel(nextLevelExp)
            account.nextLevelExp = nextLevelExp
        }
        account.exp = newExp
        account.level = level
    }

    private fun getExpForNextLevel(currentLevel: Long): Long {
        return currentLevel*2
    }


    fun setBanned(account: Account, banned: Boolean) {
        if (account.banned == banned) return
        account.banned = banned
        account.sendEvent(BannedEvent)
        account.sessions.toList().forEach {
            it.close()
        }
    }

    fun delete(account: Account) {
        cache.remove(account.accountId)
        account.sendEvent(AccountEvent.Remove(account))
        account.onDestroyTasks.forEach { it() }
    }

    override fun tick(tick: TickContext) {
        synchronized(cache) {
            forceForIndexed(cache.values) { account ->
                try {
                    account.tick(tick)
                } catch (e: Throwable) {
                    log.error(e, { "accountId=${account.accountId}, time=$tick" })
                }
            }
        }
    }

    fun clear() {
        cache.clear()
    }
}