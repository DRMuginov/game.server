package game.server.services

import game.server.domain.entity.AccountAuthEntity
import game.server.domain.entity.AccountAuthRepository
import game.server.game.Account
import game.server.game.GameException
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.core.scheduler.Scheduler
import java.util.*
import javax.annotation.PostConstruct
import javax.persistence.EntityManager

@Service
class AccountAuthService(
        val accountAuthRepository: AccountAuthRepository,
        val accountService: AccountService,
        val entityManager: EntityManager,
        //val authGpgService: AuthGpgService,
        //val gameCenterService: AuthGameCenterService,
        val databaseManager: DatabaseManager,
        @Qualifier("thirdPartyServiceScheduler")
        val serviceScheduler: Scheduler,
        @Qualifier("databaseScheduler")
        val databaseScheduler: Scheduler
) {

    @PostConstruct
    fun postConstruct() {
        accountService.genId.set(accountAuthRepository.getMaxAccountId() + 1L)
    }

    fun <T> async(callable: () -> T): Mono<T> {
        return Mono.fromCallable(callable)/*.publishOn(databaseScheduler)*/
    }

    fun regLogin(params: AuthLogin): Mono<Account> {
        return async {
            val createQuery = entityManager.createNativeQuery("""
            SELECT * FROM account_auth a WHERE a.auth_type = :auth_type AND a.json->>'login'=:login
            """, AccountAuthEntity::class.java).apply {
                setParameter("auth_type", AccountAuthType.login.toString())
                setParameter("login", params.login)
            }
            checkLogin(params.login)
            checkPassword(params.password)
            val singleResult = createQuery.resultList.firstOrNull()
            if (singleResult != null) {
                throw GameException("login already exists")
            }
            val accountAuthEntity = databaseManager.save(accountAuthRepository, AccountAuthEntity(
                    accountId = null,
                    auth_type = AccountAuthType.login
            ), params)
            val account = createAccount(accountAuthEntity, params.login)
            account
        }.publishOn(databaseScheduler)
    }

    private fun createAccount(accountAuthEntity: AccountAuthEntity, name: String): Account {
        val account = accountService.createAccount()
        accountService.setName(account, name)
        accountAuthEntity.accountId = account.accountId
        accountAuthRepository.save(accountAuthEntity)
        return account
    }

    fun authLogin(params: AuthLogin): Mono<Account> {
        return async<Account> {
            val entityDB = databaseManager.nativeQuerySingle("""
            select * from account_auth a where a.auth_type = :auth_type and a.json->>'login'=:login
            """, AccountAuthEntity::class.java, AuthLogin::class.java) { query ->
                query.setParameter("auth_type", AccountAuthType.login.toString())
                query.setParameter("login", params.login)
            }
            if (entityDB != null) {
                val entity = entityDB.entity
                if (passwordIsSame(entityDB.body.password, params.password)) {
                    val accountId = entity.accountId
                    if (accountId != null) {
                        val account = accountService.getAccount(accountId) ?: createAccount(entity, params.login)
                        return@async account
                    }
                }
            }
            throw GameException("Wrong login or password")
        }.publishOn(databaseScheduler)
    }

    private fun checkPassword(password: String) {
        if (password.isEmpty()) {
            throw GameException("password is empty")
        }
    }

    private fun checkLogin(login: String) {
        if (login.isEmpty()) {
            throw GameException("login is empty")
        }
    }

    private fun passwordIsSame(password1: String, password2: String): Boolean {
        return Objects.equals(password1, password2)
    }

    /*fun authGpg(params: AuthGpgService.AuthGpgParams): Mono<Account> {
        return Mono.just(params)
                .map { params ->
                    val player = authGpgService.authPlayer(params)
                    AuthGpg(playerId = player.playerId, displayName = player.displayName, avatarImageUrl = player.avatarImageUrl)
                }
                .publishOn(serviceScheduler)
                .map { authGpg ->
                    auth(AccountAuthType.gpg, "playerId", authGpg.playerId, AuthGpg::class.java, authGpg)
                }.publishOn(databaseScheduler)
    }*/

    /*fun authGameCenter(params: AuthGameCenterService.AuthGameCenterParams): Mono<Account> {
        return Mono.just(params)
                .flatMap {
                    val verified = gameCenterService.verify(params)
                    if (verified) {
                        Mono.just(AuthGameCenter(playerId = params.playerId))
                    } else {
                        Mono.error(GameException("not verified"))
                    }
                }
                .publishOn(serviceScheduler)
                .map { authData ->
                    auth(AccountAuthType.game_center, "playerId", params.playerId, AuthGameCenter::class.java, authData)
                }.publishOn(databaseScheduler)
    }*/

    fun <AUTH_DATA : Any> auth(accountAuthType: AccountAuthType, id: String, value: String, authDataType: Class<AUTH_DATA>, authData: AUTH_DATA): Account {
        val entityDB = databaseManager.nativeQuerySingle("""
            select * from account_auth a where a.auth_type = :auth_type and a.json->>'$id'=:$id
            """, AccountAuthEntity::class.java, authDataType) { query ->
            query.setParameter("auth_type", accountAuthType.toString())
            query.setParameter("$id", value)
        }
        val entity: AccountAuthEntity
        if (entityDB == null) {
            entity = databaseManager.save(accountAuthRepository, AccountAuthEntity(accountId = null, auth_type = accountAuthType), authData)
        } else {
            val storedAuthData = entityDB.body
            if (!Objects.equals(authData, storedAuthData)) {
                entity = databaseManager.save(accountAuthRepository, entityDB.entity, authData)
            } else {
                entity = entityDB.entity
            }
        }
        val accountId = entity.accountId
        val existingAccount = if (accountId != null) {
            accountService.getAccount(accountId)
        } else {
            null
        }
        val account = existingAccount ?: createAccount(entity, value)
        return account
    }

    fun delete(account: Account) {
        databaseManager.execute { accountAuthRepository.deleteByAccountId(account.accountId) }
    }

    fun clear() {
        async {
            accountAuthRepository.deleteAll()
        }.block()
    }

}

enum class AccountAuthType {
    login,
    gpg,
    game_center,
}

data class AuthLogin(
        val login: String = "",
        val password: String = ""
)

data class AuthGpg(
        val playerId: String = "",
        val displayName: String? = null,
        val avatarImageUrl: String? = null
)

data class AuthGameCenter(
        val playerId: String = ""
)