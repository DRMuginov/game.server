package game.server.services

import game.server.game.util.ITick
import game.server.game.util.TickContext
import game.server.game.util.TickContextImpl
import mu.KotlinLogging
import org.springframework.stereotype.Service
import reactor.core.publisher.DirectProcessor
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.concurrent.BlockingQueue
import java.util.concurrent.CompletableFuture
import java.util.concurrent.LinkedBlockingQueue

@Service
class GameLoopService(
        val timeService: TimeService
) {

    val log = KotlinLogging.logger { }

    val tickPeriod: Long = 100L
    private val queue: BlockingQueue<() -> Unit> = LinkedBlockingQueue()
    private val tickContext: TickContextImpl = TickContextImpl()
    private var loopContext: LoopContext? = null
    private val tickEmitter = DirectProcessor.create<Unit>()

    var tickListeners: Array<ITick> = emptyArray()

    val tick: TickContext
        get() = tickContext

    fun start() {
        stop()

        val ctx = LoopContext(run = ::loop)
        loopContext = ctx

        Thread(ctx).apply {
            name = "game"
            isDaemon = false
            start()
        }
    }

    fun stop() {
        loopContext?.stop()?.block()
    }

    fun <T> scheduleMono(function: () -> T): Mono<T> {
        val future = CompletableFuture<T>()
        schedule {
            try {
                future.complete(function())
            } catch (e: Throwable) {
                future.completeExceptionally(e)
            }
        }
        return Mono.fromFuture(future)
    }

    fun schedule(function: () -> Unit) {
        queue.add(function)
    }

    fun onTick(): Flux<Unit> {
        return tickEmitter
    }

    private fun loop(ctx: LoopContext) {
        var t0 = timeService.currentTimeMillis()
        resetTimeContext()
        while (!ctx.stop) {
            tick()
            val t1 = timeService.currentTimeMillis()
            val timePassed = t1 - t0
            val sleepTime = tickPeriod - timePassed
            //log.debug { "tick ($timePassed)" }
            if (sleepTime > 0) {
                Thread.sleep(sleepTime)
            }
            t0 = t1
        }
    }

    fun tick() {
        try {
            processMessages()
            updateTimeContext()
            tickListeners.forEach { it.tick(tickContext) }
            tickEmitter.onNext(Unit)
        } catch (e: Throwable) {
            log.error(e, {})
        }
    }

    private fun resetTimeContext() {
        tickContext.time = timeService.currentTimeMillis()
        tickContext.deltaTime = 0
    }

    private fun updateTimeContext() {
        val now = timeService.currentTimeMillis()
        val delta = now - tickContext.time
        tickContext.deltaTime = delta.toInt()
        tickContext.time = now
        tickContext.frame += 1
    }

    fun processMessages(): Int {
        var processedMessages = 0
        while (true) {
            val task = queue.poll() ?: break
            task()
            processedMessages++
        }
        return processedMessages
    }

    class LoopContext(val run: (LoopContext) -> Unit) : () -> Unit {

        var stop: Boolean = false
            private set
        val stopFuture = CompletableFuture<Unit>()

        override fun invoke() {
            run(this)
            stopFuture.complete(Unit)
        }

        fun stop(): Mono<Unit> {
            stop = true
            return Mono.fromFuture(stopFuture)
        }
    }
}