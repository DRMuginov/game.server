package game.server.services

import org.springframework.stereotype.Service
import reactor.core.Disposable
import reactor.core.scheduler.Scheduler

@Service
class GameLoopScheduler(
        private val gameLoopService: GameLoopService
) : Scheduler {

    private val worker = Worker()

    private fun scheduleInner(task: Runnable): Disposable {
        return gameLoopService.scheduleMono { task.run() }.subscribe()
    }

    override fun schedule(task: Runnable): Disposable {
        return scheduleInner(task)
    }

    override fun createWorker(): Scheduler.Worker {
        return worker
    }

    inner class Worker: Scheduler.Worker {
        override fun schedule(task: Runnable): Disposable {
            return scheduleInner(task)
        }

        override fun dispose() {
        }

    }
}