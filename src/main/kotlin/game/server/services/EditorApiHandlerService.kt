package game.server.services

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.NullNode
import com.fasterxml.jackson.databind.node.ObjectNode
import game.server.game.*
import game.server.game.api.EditorApi
import game.server.game.api.data.*
import game.server.game.api.exceptions.ApiException
import game.server.game.api.utils.ApiMethod
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class EditorApiHandlerService(
        val databaseManager: DatabaseManager,
        val configService: ConfigService,
        val gameLoopScheduler: GameLoopScheduler,
        val gameService: GameService,
        val accountService: AccountService,
        val accountAuthService: AccountAuthService
) : ApiMethodHandler {

    private val handlers = mutableMapOf<String, MethodHandlerMono<*, *>>()
    private val methods = mutableMapOf<String, ApiMethod<*, *>>()

    @Suppress("UNCHECKED_CAST")
    override fun <PARAMS, RESULT> handle(apiMethod: ApiMethod<PARAMS, RESULT>, params: PARAMS): Mono<RESULT> {
        val apiMethodHandler = handlers[apiMethod.name] as MethodHandlerMono<PARAMS, RESULT>?
        return if (apiMethodHandler == null) {
            Mono.error(NotImplementedError("method ${apiMethod.name} not implemented"))
        } else {
            apiMethodHandler(params)
        }
    }

    init {
//        addMono(EditorApi.SAVE_GENERIC, { params -> Mono.fromCallable { saveGeneric(params) } })
//        addMono(EditorApi.GET_GENERIC, { params -> Mono.fromCallable { getGeneric(params) } })
//        addMono(EditorApi.GET_ALL_GENERIC, { params -> Mono.fromCallable { getAllGeneric(params) } })
        addMono(EditorApi.PING, { Mono.empty() })
        addMono(EditorApi.GET_ACCOUNT_INFO, ::getAccountInfo)
        addMono(EditorApi.GET_ACCOUNTS, ::getAccounts)
        addMono(EditorApi.ACCOUNT_DELETE, ::accountDelete)
        addMono(EditorApi.ACCOUNT_ADD_POINTS, ::accountAddPoints)
        addMono(EditorApi.ACCOUNT_ADD_EXP, ::accountAddExp)
    }

    private fun getAccountInfo(request: GetAccountInfoRequest): Mono<GetAccountInfoResponse> {
        return getAccountMono(request.accountId)
                .map(::toAccountInfoResponse)
                .publishOn(gameLoopScheduler)
    }

    private fun toAccountInfoResponse(account: Account): GetAccountInfoResponse {
        return GetAccountInfoResponse(
                accountId = account.accountId,
                name = account.name,
                level = account.level,
                exp = account.exp,
                points = account.points,
                bonuses = account.bonuses.values().map { BonusData(it.type, it.count, it.limit) }
        )
    }

    private fun accountAddExp(request: AccountAddRequest): Mono<Unit> {
        return getAccountMono(request.accountId)
                .map { account ->
                    accountService.addExp(account, request.count)
                }
                .publishOn(gameLoopScheduler)
    }

    private fun accountAddPoints(request: AccountAddRequest): Mono<Unit> {
        return getAccountMono(request.accountId)
                .map { account ->
                    accountService.addPoints(account, request.count)
                }
                .publishOn(gameLoopScheduler)
    }

    private fun accountDelete(request: AccountDeleteRequest): Mono<Unit> {
        return getAccountMono(request.accountId)
                .map { account ->
                    accountService.delete(account)
                    accountAuthService.delete(account)
                    account.sessions.toList().forEach { it.close() }
                }
                .publishOn(gameLoopScheduler)
    }

    private fun getAccounts(request: GetAccountsRequest): Mono<GetAccountsResponse> {
        return getAccountsFlux(request)
                .collectList()
                .map { accounts ->
                    GetAccountsResponse(accounts.drop(request.offset).take(request.count).map(::toAccountShortInfo), accounts.size)
                }
    }

    fun getAccountsFlux(request: GetAccountsRequest): Flux<Account> {
        return Flux
                .fromIterable(accountService.getAllAccounts())
                .publishOn(gameLoopScheduler)
                .let { createAccountFilter(it, request) }
                .sort(createAccountComparator(request.sort))
    }

    private fun createAccountComparator(sort: SortDataAccountOrderField?): Comparator<Account> {
        if (sort?.orders == null || sort.orders.isEmpty()) {
            return defaultAccountComparator()
        } else {
            return sort.orders
                    .map(::createAccountComparator)
                    .reduce { acc, comparator -> acc.then(comparator) }
                    .then(defaultAccountComparator())
        }
    }

    private fun createAccountComparator(order: SortOrderData<AccountOrderField>): Comparator<Account> {
        return createAccountComparator(order.field, order.dir)
    }

    private fun createAccountComparator(orderBy: AccountOrderField?, orderDir: OrderDirection): Comparator<Account> {
        val comparator = createAccountComparator(orderBy)
        return when (orderDir) {
            OrderDirection.ASC -> comparator
            OrderDirection.DESC -> comparator.reversed()
        }
    }

    private fun createAccountComparator(orderBy: AccountOrderField?): Comparator<Account> {
        return when (orderBy) {
            null -> defaultAccountComparator()
            AccountOrderField.account_id -> defaultAccountComparator()
            AccountOrderField.level -> Comparator.comparingInt<Account> { it.level }
            AccountOrderField.blocked -> Comparator.comparing<Account, Boolean> { it.banned }
            AccountOrderField.account_name -> Comparator.comparing<Account, String> { it.name }
        }
    }

    private fun defaultAccountComparator() = Comparator.comparingLong(Account::accountId)


    private fun createAccountFilter(entry: Flux<Account>, request: GetAccountsRequest): Flux<Account> {
        var flux = entry
        if (request.name != null) {
            val likeString = request.name.replace("*", ".*").let { ".*$it.*" }
            flux = flux.filter { like(it.name, likeString) }
        }
        if (request.level != null) {
            if (request.level.from != null) {
                flux = flux.filter { it.level >= request.level.from }
            }
            if (request.level.to != null) {
                flux = flux.filter { it.level <= request.level.to }
            }
        }
        if (request.blocked != null) {
            flux = flux.filter { it.banned == request.blocked }
        }
        return flux
    }

    private fun like(baseString: String, likeString: String): Boolean {
        return baseString.matches(likeString.toRegex(RegexOption.IGNORE_CASE))
    }

    private fun toAccountShortInfo(account: Account): AccountAdminShortInfo {
        return AccountAdminShortInfo(account.accountId, account.name, account.level, account.banned)
    }

    private fun getAccountMono(accountId: Long): Mono<Account> {
        return Mono
                .just(accountId).flatMap {
                    val account = accountService.getAccount(accountId)
                    if (account == null) {
                        Mono.error(ApiException(ApiError.NOT_FOUND, "account with id: $accountId"))
                    } else {
                        Mono.just(account)
                    }
                }
    }

    fun getApiMethodByName(methodName: String): ApiMethod<*, *>? {
        return methods[methodName]
    }

    final fun <PARAMS, RESULT> add(apiMethod: ApiMethod<PARAMS, RESULT>, handler: MethodHandler<PARAMS, RESULT>) {
        addMono(apiMethod, { Mono.fromCallable { handler(it) } })
    }

    final fun <PARAMS, RESULT> addMono(apiMethod: ApiMethod<PARAMS, RESULT>, handler: MethodHandlerMono<PARAMS, RESULT>) {
        handlers[apiMethod.name] = handler
        methods[apiMethod.name] = apiMethod
    }

    fun clear() {
        //editorRepository.deleteAll()
    }

}

typealias MethodHandlerMono<PARAMS, RESULT> = (PARAMS) -> Mono<RESULT>
typealias MethodHandler<PARAMS, RESULT> = (PARAMS) -> RESULT

interface ApiMethodHandler {
    fun <PARAMS, RESULT> handle(apiMethod: ApiMethod<PARAMS, RESULT>, params: PARAMS): Mono<RESULT>
}