package game.server.services

import com.fasterxml.jackson.databind.JsonNode
import game.server.game.*
import game.server.game.api.GameApiEvents
import game.server.game.api.data.*
import game.server.game.api.utils.ApiEventData
import game.server.game.util.ICollectionEvent
import game.server.game.util.ICollectionEventClear
import game.server.game.util.IListItemEventAdd
import game.server.game.util.IListItemEventRemove
import game.server.services.utils.ChannelSession
import game.server.services.utils.HashMapListed
import game.server.utils.createOrAdd
import game.server.utils.getOrCreateListValue
import org.springframework.stereotype.Service
import java.util.concurrent.atomic.AtomicLong

@Service
class GameService(
        val accountService: AccountService
) {

    val cache = HashMapListed<Long, GameRoom>()

    val genId = AtomicLong(0)

    fun toApiEvent(e: GameEvent, send: (ApiEventData<*>) -> Unit) {
        when (e) {
            is ApiEventData<*> -> send(e)
            is AccountEvent.Login ->
                send(GameApiEvents.LOGIN.create(LoginData(e.account.accountId)))
            is Account.UpdateEvent ->
                send(GameApiEvents.UPDATE_ACCOUNT.create(toAccountUpdateData(e)))
            is AccountEvent.StartGame ->
                send(GameApiEvents.START_GAME.create(GameRoomId(e.account.gameRoomId!!)))
            is AccountEvent.SendData ->
                send(GameApiEvents.SEND_DATA.create(SendData(e.message)))
            is AccountEvent.EndGame ->
                send(GameApiEvents.END_GAME.create(GameRoomId(e.gameRoomId)))
        }
    }

    fun toAccountData(account: Account): AccountData {
        return AccountData(
                accountId = account.accountId,
                name = account.name,
                level = account.level,
                exp = account.exp,
                nextLevelExp = account.nextLevelExp,
                points = account.points,
                gameRoomId = account.gameRoomId
        )
    }

    fun toAccountUpdateData(e: Account.UpdateEvent): AccountUpdateData {
        return AccountUpdateData(
                accountId = e.account.accountId
        ).also { data ->
            val u = e.update
            val field = u.field
            when (field) {
                Account.Update.level -> data.level = u.valueAs(field)
                Account.Update.exp -> data.exp = u.valueAs(field)
                Account.Update.expAdded -> data.createOrAdd(AccountUpdateData::expAdded, u.valueAs(field))
                Account.Update.points -> data.points = u.valueAs(field)
                Account.Update.pointsAdded -> data.points = u.valueAs(field)
                Account.Update.nextLevelExp -> data.nextLevelExp = u.valueAs(field)
                Account.Update.session -> data.createOrAdd(AccountUpdateData::sessions, toListChangeData(u.valueAs(field), ::toAccountSessionData))
                Account.Update.bonus -> u.valueAs(field).let {
                    data.getOrCreateListValue(AccountUpdateData::bonuses, { ArrayList() }, { it.type == it.type }, { BonusUpdateData(it.type) }).apply {
                        it.count?.let { count = it }
                        it.limit?.let { limit = it }
                    }
                }
                else -> TODO()
            }
        }
    }

    fun toAccountSessionData(session: ChannelSession): AccountSessionData {
        return AccountSessionData(session.info)
    }

    fun <T, TD, UD> toListChangeData(change: ICollectionEvent<T>, mapItem: (T) -> TD): ListChangeData<TD, UD> {
        when (change) {
            is IListItemEventAdd -> return ListChangeData.Add(change.index, mapItem(change.item))
            is IListItemEventRemove -> return ListChangeData.Remove(change.index)
            is ICollectionEventClear -> return ListChangeData.Clear()
        }
        TODO(change.javaClass.simpleName)
    }

    fun createGameRoom(accountId1: Long, accountId2: Long): GameRoom {
        return createGameRoom(genId(), accountId1, accountId2)
    }

    fun genId(): Long {
        return genId.incrementAndGet()
    }

    private fun createGameRoom(gameRoomId: Long, accountId1: Long, accountId2: Long): GameRoom {
        val gameRoom = GameRoom().apply {
            this.gameRoomId = gameRoomId
            this.accountId1 = accountId1
            this.accountId2 = accountId2
        }
        register(gameRoom)
        return gameRoom
    }

    fun register(gameRoom: GameRoom) {
        cache[gameRoom.gameRoomId] = gameRoom
    }

    fun getGameRoom(gameRoomId: Long): GameRoom? {
        val cachedAccount = cache[gameRoomId]
        if (cachedAccount != null) return cachedAccount
        return null
    }

    fun getInfo(account: Account, channelSession: ChannelSession, params: Unit): AccountData {
        return toAccountData(account)
    }

    fun addExp(account: Account, channelSession: ChannelSession, params: AddData) {
        accountService.updateExp(account, params)
    }

    fun addPoints(account: Account, channelSession: ChannelSession, params: AddData) {
        accountService.updatePoints(account, params)
    }


    fun addBonuses(account: Account, channelSession: ChannelSession, params: AddBonusesData) {
        for (resource in params.resources) {
            changeBonusCount(account, resource.type, resource.count)
        }
    }

    fun changeBonusCount(account: Account, resourceType: BonusType, by: Int) {
        val resource = account.bonuses[resourceType]
        resource.count += by
    }

    fun getOnline(account: Account, channelSession: ChannelSession, params: Unit): GetOnlineAccountsData{
        val accounts = accountService.getAllAccounts()
        val players = accounts.filter { (it.accountId != account.accountId) && (it.gameRoomId == null)}
        return GetOnlineAccountsData(players.map { toAccountData(it) })
    }

    fun startGame(account: Account, channelSession: ChannelSession, params: AccountId) {
        val account2 = accountService.getAccount(params.accountId)
        if(account2 != null) {
            val gameRoom = createGameRoom(account.accountId, account2.accountId)
            account.gameRoomId = gameRoom.gameRoomId
            account2.gameRoomId = gameRoom.gameRoomId
            sendAllSessions(account,AccountEvent.StartGame(account))
            sendAllSessions(account2,AccountEvent.StartGame(account2))
        } else
            throw GameException("player not found")
    }

    fun endGame(account: Account, channelSession: ChannelSession, params: GameRoomId) {
        val gameRoom = getGameRoom(params.gameRoomId)
        if(gameRoom != null){
            val players = accountService.getPlayers(gameRoom)
                for(player in players)
                    if (player != null) {
                        sendAllSessions(player,AccountEvent.EndGame(player, player.gameRoomId!!))
                        player.gameRoomId = null
                    }
            cache.remove(gameRoom.gameRoomId)
        } else
            throw GameException("game room not found")
    }

    fun sendAllSessions(account: Account, gameEvent: GameEvent){
        for(session in account.sessions)
            session.send(gameEvent)
    }

    fun sendData(account: Account, channelSession: ChannelSession, params: JsonNode) {
        val secondPlayer = getSecondPlayer(account)
        if(secondPlayer != null)
            sendAllSessions(secondPlayer,AccountEvent.SendData(secondPlayer, params))
        else
            throw GameException("another player connection not found")
    }

    fun getSecondPlayer(account: Account): Account?{
        if(account.gameRoomId != null) {
            val gameRoom = getGameRoom(account.gameRoomId!!)
            if (gameRoom != null) {
                val players = accountService.getPlayers(gameRoom)
                for(player in players)
                    if ((player != null) && (player.accountId != account.accountId))
                        return player
                return null
            } else
                return null
        }else
            throw GameException("account dont have game room id")
    }

}
