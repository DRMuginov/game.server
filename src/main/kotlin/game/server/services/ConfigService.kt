package game.server.services

import com.fasterxml.jackson.annotation.JsonPropertyDescription
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.jacksonTypeRef
/*import game.server.utils.JsonEntity
import game.server.utils.ReactiveJsonRepository*/
import mu.KotlinLogging
import org.springframework.beans.BeanUtils
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties

@Service
class ConfigService(
        /*@Qualifier("yamlMapper")
        private val yamlMapper: ObjectMapper,
        val gameObjectTemplatesService: GameObjectTemplatesService,
        val locationBuilderService: LocationBuilderService,
        val gameSettings: GameSettings,
        val shopItemRepository: ShopItemRepositoryImpl,
        @Qualifier("configJsonRepository")
        val configJsonRepository: ReactiveJsonRepository,
        val factionService: FactionService*/
) {

    val log = KotlinLogging.logger { }
    val configs: MutableMap<String, ConfigHandler<*>> = mutableMapOf()

    /*fun init() {
        registerConfigs()
        reloadAllConfigs()
    }

    private fun registerConfigs() {
        registerConfig(Configs.GAME_SETTINGS) { copyProperties(it, gameSettings) }
        registerConfig(Configs.OBJECT_DATABASE) { loadObjectDatabase(it) }
        registerConfig(Configs.TERRITORIES) { loadTerritories(it) }
        registerConfig(Configs.SHOP) { loadShopItems(it) }
        registerConfig(Configs.FACTIONS) { loadFactions(it) }
    }

    private fun loadFactions(list: List<FactionDefinition>) {
        factionService.replaceAll(list)
    }

    private fun loadShopItems(list: List<ShopItem>) {
        shopItemRepository.map = list.associateBy { it.id }.toMutableMap()
    }

    private inline fun <reified T : Any> registerConfig(configDescription: ConfigDescription<T>, noinline apply: (T) -> Unit) {
        val config = ConfigHandler(configDescription = configDescription, apply = apply)
        configs[config.configDescription.name] = config
    }

    fun getConfigByName(resourceName: String): ConfigHandler<*>? {
        return configs[resourceName]
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : Any> getConfigByType(type: Class<T>): ConfigHandler<T>? {
        return configs.values
                .filter { it.configDescription.typeReference.type == type }
                .map { it as ConfigHandler<T> }
                .firstOrNull()
    }

    fun <T : Any> getConfigNameByType(type: Class<T>): String? {
        return getConfigByType(type)?.configDescription?.name
    }

    fun reloadConfig(configName: String) {
        reloadConfig(getConfigByName(configName)!!)
    }

    @Suppress("UNCHECKED_CAST")
    fun reloadConfig(configHandler: ConfigHandler<*>) {
        try {
            log.info { "reload config ${configHandler.configDescription.name}" }
            val value = loadConfig(configHandler.configDescription)
            if (value != null) {
                configHandler as ConfigHandler<Any>
                configHandler.apply(value)
            }
        } catch (e: Throwable) {
            log.error(e) { configHandler.configDescription.name }
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun reloadAllConfigs() {
        configs.values.forEach { reloadConfig(it as ConfigHandler<Any>) }
    }

    private fun <T : Any> copyProperties(fromInstance: T, toInstance: T) {
        BeanUtils.copyProperties(fromInstance, toInstance)
    }

    fun <T : Any> loadConfig(configDescription: ConfigDescription<T>): T? {
        try {
            return yamlMapper.readValue(yamlMapper.treeAsTokens(getConfigNode(configDescription.name)), configDescription.typeReference)
        } catch (e: Throwable) {
            throw Exception(configDescription.name, e)
        }
    }

    fun <T : Any> applyConfig(configHandler: ConfigHandler<T>, json: JsonNode) {
        val treeToValue: T = yamlMapper.readValue(yamlMapper.treeAsTokens(json), configHandler.configDescription.typeReference)
        configHandler.apply(treeToValue)
    }

    fun saveConfigJson(json: ObjectNode) {
        for ((key, value) in json.fields()) {
            val config = getConfigByName(key)
            if (config == null) {
                log.warn { "bad config name: $key" }
                continue
            }
            if (value !is ObjectNode) {
                log.warn { "bad config value: $value" }
                continue
            }
            saveConfigJson(key, value)
        }
    }

    fun saveConfigJson(key: String, json: JsonNode) {
        writeJsonNode(key, json)
    }

    fun getAllConfigs(): List<ConfigHandler<*>> {
        return configs.values.toList()
    }

    private fun loadObjectDatabase(input: List<IGameObjectTemplate>) {
        gameObjectTemplatesService.replaceAll(input)
    }

    fun loadTerritories(input: List<TerritoryTemplate>) {
        locationBuilderService.replaceAll(input)
    }

    fun getConfigNode(resourceName: String): JsonNode? {
        return configJsonRepository.findById(resourceName).map { it.node }.blockOptional().orElse(null)
    }

    fun getConfigNode(configHandlerInstance: ConfigHandler<*>): JsonNode? {
        return getConfigNode(configHandlerInstance.configDescription.name)
    }

    private fun writeJsonNode(resourceName: String, json: JsonNode) {
        configJsonRepository.save(JsonEntity(resourceName, json)).block()
    }

    fun getConfigDocumentByName(name: String): JsonNode? {
        return getConfigNode(name)
    }*/

}

data class ConfigHandler<T : Any>(
        val configDescription: ConfigDescription<T>,
        val apply: (T) -> Unit
)

data class ConfigDescription<T>(val name: String, val typeReference: TypeReference<T>)

object Configs {
    /*@JsonPropertyDescription("Бонусы")
    val BONUS = create<List<ShopItem>>("shop")


    fun values(): List<ConfigDescription<*>> {
        return Configs::class
                .memberProperties
                .mapNotNull { it as? KProperty1<Configs, ConfigDescription<*>> }
                .map { it.call(Configs) }
    }

    private inline fun <reified T : Any> create(name: String): ConfigDescription<T> {
        return ConfigDescription(name, jacksonTypeRef())
    }*/
}