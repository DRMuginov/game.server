package game.server.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.annotation.ComponentScan

@ComponentScan("game.server")
@SpringBootApplication
class WebApplication

fun main(args: Array<String>) {
    SpringApplicationBuilder(WebApplication::class.java).run(*args)
}


