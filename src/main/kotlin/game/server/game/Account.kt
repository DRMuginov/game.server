package game.server.game

import com.fasterxml.jackson.databind.JsonNode
import game.server.game.api.data.FieldsHolder
import game.server.game.api.data.IFieldValue
import game.server.game.util.*
import game.server.services.utils.ChannelSession
import game.server.utils.forceForIndexed
import reactor.core.publisher.DirectProcessor
import reactor.core.publisher.Flux
import java.util.concurrent.CopyOnWriteArrayList
import game.server.utils.Action1

class Account : ITick, FieldsUpdateSupport<Account.Update> {

    var accountId: Long = 0
    var name: String = "<none>"
        set(value) {
            if (field == value) return
            field = value
            sendUpdate(Update.name, value)
        }
    var exp: Long = 0
        set(value) {
            if (field == value) return
            field = value
            sendUpdate(Update.exp, value)
        }
    var nextLevelExp: Long = 50
        set(value) {
            if (field == value) return
            field = value
            sendUpdate(Update.nextLevelExp, value)
        }
    var level: Int = 1
        set(value) {
            if (field == value) return
            field = value
            sendUpdate(Update.level, value)
        }
    var points: Long = 0
        set(value) {
            if (field == value) return
            field = value
            sendUpdate(Update.points, value)
        }
    var banned: Boolean = false

    var gameRoomId: Long? = null

    val bonuses: AccountBonuses = AccountBonuses { sendUpdate(Update.bonus, it) }

    val sessions: MutableList<ChannelSession> = ObservableListOnly<ChannelSession>(CopyOnWriteArrayList()).addObserver { sendUpdate(Update.session, it) }

    var onDestroyTasks = emptyList<() -> Unit>()

    private val eventEmitter = DirectProcessor.create<GameEvent>()

    fun sendEvent(event: GameEvent) {
        eventEmitter.onNext(event)
    }

    fun eventPublisher(): Flux<GameEvent> {
        return eventEmitter
    }

    override fun sendUpdate(change: IFieldValue<Update, *>) {
        sendEvent(UpdateEvent(this, change))
    }

    fun expAdded(exp: Long) {
        sendUpdate(Update.expAdded, exp)
    }

    fun pointsAdded(points: Long) {
        sendUpdate(Update.pointsAdded, points)
    }

    interface IAccountEvent : GameEvent {
        val account: Account
    }

    object Update : FieldsHolder<Update>() {
        val name = field<String>("name")
        val exp = field<Long>("exp")
        val level = field<Int>("level")
        val nextLevelExp = field<Long>("nextLevelExp")
        val points = field<Long>("points")
        val expAdded = field<Long>("expAdded")
        val pointsAdded = field<Long>("pointsAdded")
        val bonus = field<BonusUpdate>("effect")
        val session = field<ICollectionEvent<ChannelSession>>("session")
    }

    fun addBonuses(list: List<BonusCount>) {
        for (resource in list)
            changeBonusCount(resource.type, resource.count)
    }

    fun changeBonusCount(bonusType: BonusType, by: Int) {
        val bonus = bonuses[bonusType]
        bonus.count += by
    }

    override fun tick(tick: TickContext) {
        synchronized(sessions) {
            forceForIndexed(sessions) { channelSession ->
                channelSession.push()
            }
        }
    }

    data class UpdateEvent(override val account: Account, val update: IFieldValue<Update, *>) : Account.IAccountEvent
}

object AccountEvent {
    data class Login(override val account: Account) : Account.IAccountEvent
    data class Logout(override val account: Account) : Account.IAccountEvent
    data class Remove(override val account: Account) : Account.IAccountEvent
    data class StartGame(override val account: Account) : Account.IAccountEvent
    data class SendData(override val account: Account, val message: JsonNode) : Account.IAccountEvent
    data class EndGame(override val account: Account, val gameRoomId: Long) : Account.IAccountEvent
}

class AccountBonuses(
        private val observer: (BonusUpdate) -> Unit
) {
    val bonuses: MutableMap<BonusType, BonusStorage> = mutableMapOf()
    val limits = ObservableListOnly<OwnedValue<BonusCount>>().addObserver(::onLimitsUpdate)

    private fun create(bonusType: BonusType): BonusStorage {
        return BonusStorage(bonusType, observer)
    }

    operator fun get(bonusType: BonusType): BonusStorage {
        return bonuses.computeIfAbsent(bonusType, { create(it) })
    }

    private fun onLimitsUpdate(listChange: ICollectionEvent<OwnedValue<BonusCount>>) {
        when (listChange) {
            is ICollectionItemEventAdd -> get(listChange.item.value.type).limit += listChange.item.value.count
            is ICollectionItemEventRemove -> get(listChange.item.value.type).limit -= listChange.item.value.count
        }
    }

    fun values() = bonuses.values
    fun limits() = limits
}

data class BonusStorage(
        val type: BonusType,
        val observer: Action1<BonusUpdate>
) {
    var count: Int = 0
        set (value) {
            if (field == value) return
            field = value
            observer(BonusUpdate(type, count = value))
        }
    var limit: Int = 10
        set (value) {
            if (field == value) return
            field = value
            observer(BonusUpdate(type, limit = value))
        }
}