package game.server.game

import com.fasterxml.jackson.annotation.JsonPropertyDescription

interface ILimitedCount {
    @get:JsonPropertyDescription("Текущее количество")
    val count: Int
    @get:JsonPropertyDescription("Доступный максимум")
    val limit: Int
}

interface ILimitedCountOptional {
    @get:JsonPropertyDescription("Текущее количество")
    val count: Int?
    @get:JsonPropertyDescription("Доступный максимум")
    val limit: Int?
}

data class BonusData(
        val type: BonusType,
        override var count: Int = 0,
        override var limit: Int = 0
): ILimitedCount

data class BonusUpdateData(
        val type: BonusType,
        override var count: Int? = null,
        override var limit: Int? = null
): ILimitedCountOptional

data class BonusUpdate (
        val type: BonusType,
        override var count: Int? = null,
        override var limit: Int? = null
): ILimitedCountOptional