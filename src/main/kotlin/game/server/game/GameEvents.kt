package game.server.game

import com.fasterxml.jackson.databind.JsonNode

interface GameEvent

data class EchoEvent(val name: String = "", val data: JsonNode? = null) : GameEvent

data class DebugEvent(val messsage: String) : GameEvent

object BannedEvent : GameEvent