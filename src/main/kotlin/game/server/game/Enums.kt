package game.server.game

import com.fasterxml.jackson.annotation.JsonClassDescription
import com.fasterxml.jackson.annotation.JsonPropertyDescription
import com.fasterxml.jackson.annotation.JsonTypeInfo

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NONE,
        defaultImpl = BonusType::class
)
interface IBonusType

@JsonClassDescription("Тип бонус")
enum class BonusType(val limited: Boolean = true): IBonusType {
    @JsonPropertyDescription("Бонус 1")
    BONUS1(false),
    @JsonPropertyDescription("Бонус 2")
    BONUS2(false),
    @JsonPropertyDescription("Бонус 3")
    BONUS3(false)
}

