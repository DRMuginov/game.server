package game.server.game

data class GameRoom(
    var gameRoomId: Long = 0L,

    var accountId1: Long = 0L,

    var accountId2: Long = 0L
)
