package game.server.game

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import game.server.game.entities.SourceOwner
import game.server.game.util.IObservable
import game.server.utils.Action1
import java.time.Duration

data class InstantData(
        val millis: Long
)

@JsonSerialize(using = DurationData.Serializer::class)
@JsonDeserialize(using = DurationData.Deserializer::class)
data class DurationData(
        val millis: Long
) {

    fun toDuration(): Duration {
        return Duration.ofMillis(millis)
    }

    override fun toString(): String {
        if (millis % 1000L == 0L) {
            val seconds = millis / 1000L
            return when {
                seconds % SECONDS_IN_WEEK == 0L -> "${seconds / SECONDS_IN_WEEK}w"
                seconds % SECONDS_IN_DAY == 0L -> "${seconds / SECONDS_IN_DAY}d"
                seconds % SECONDS_IN_HOUR == 0L -> "${seconds / SECONDS_IN_HOUR}h"
                seconds % SECONDS_IN_MINUTE == 0L -> "${seconds / SECONDS_IN_MINUTE}m"
                else -> "${seconds / SECONDS_IN_MINUTE}s"
            }
        }
        return "$millis"
    }

    class Serializer : JsonSerializer<DurationData>() {
        override fun serialize(value: DurationData, gen: JsonGenerator, serializers: SerializerProvider) {
            gen.writeNumber(value.millis)
        }
    }

    class Deserializer : JsonDeserializer<DurationData>() {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): DurationData {
            return DurationData.parse(p.text)
        }
    }

    companion object {
        private const val SECONDS_IN_MINUTE = 60
        private const val SECONDS_IN_HOUR = 60 * SECONDS_IN_MINUTE
        private const val SECONDS_IN_DAY = 24 * SECONDS_IN_HOUR
        private const val SECONDS_IN_WEEK = 7 * SECONDS_IN_DAY

        fun ofSeconds(seconds: Long): DurationData {
            return DurationData(seconds * 1000L)
        }

        fun parse(str: String): DurationData {
            val value = str.substring(0, str.length - 1).toLong()
            val char = str.last()
            return when (char) {
                'w' -> DurationData(value * SECONDS_IN_WEEK * 1000L)
                'd' -> DurationData(value * SECONDS_IN_DAY * 1000L)
                'h' -> DurationData(value * SECONDS_IN_HOUR * 1000L)
                'm' -> DurationData(value * SECONDS_IN_MINUTE * 1000L)
                's' -> DurationData(value * 1000L)
                else -> DurationData(str.toLong())
            }
        }
    }
}

@JsonClassDescription("Процент ([0..1] -> [0%..100%])")
@JsonSerialize(using = Percent.Serializer::class, `as` = Float::class)
@JsonDeserialize(using = Percent.Deserializer::class, `as` = Float::class)
data class Percent(
        val percent: Float
) {
    companion object {
        val ZERO = Percent(0f)

        fun of(str: String): Percent {
            val percent = if (str.endsWith('%')) {
                str.removeSuffix("%").toInt() / 100f
            } else {
                str.toFloat()
            }
            return Percent(percent)
        }

        fun example(): List<Percent> = listOf(
                Percent(0.1f),
                Percent(1f)
        )

    }

    class Serializer : JsonSerializer<Percent>() {
        override fun serialize(value: Percent, gen: JsonGenerator, serializers: SerializerProvider) {
            gen.writeNumber(value.percent)
        }

    }

    class Deserializer : JsonDeserializer<Percent>() {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Percent {
            return Percent.of(p.text)
        }
    }
}

@JsonClassDescription("Количество бонусов")
data class BonusCount(
        val type: BonusType,
        @field:JsonPropertyDescription("Количесвто")
        var count: Int = 1
)

class BonusCountObservable(
        val type: BonusType = BonusType.BONUS1,
        count: Int = 1
) : IObservable<BonusCount> {
    override var observer: Action1<BonusCount>? = null

    var count: Int = count
        set(value) {
            if (field == value) return
            field = value
            observer?.invoke(BonusCount(type, value))
        }
}

operator fun Long.minus(other: Percent): Long = this - (this * other.percent).toLong()
operator fun Long.plus(other: Percent): Long = this + (this * other.percent).toLong()

operator fun Int.minus(other: Percent): Int = this - (this * other.percent).toInt()
operator fun Int.plus(other: Percent): Int = this + (this * other.percent).toInt()

data class OwnedValue<V>(val owner: SourceOwner, val value: V)

data class ValueChanged<T>(val oldValue: T, val newValue: T)
