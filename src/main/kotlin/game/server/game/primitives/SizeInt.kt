package game.server.game.primitives

import com.fasterxml.jackson.annotation.JsonIgnore

data class SizeInt(
        val width: Int = 0,
        val height: Int = 0
) {

    @get:JsonIgnore
    val center: PointInt
        get() = PointInt(width / 2, height / 2)

    init {
        require(width >= 0) { "width must be positive" }
        require(height >= 0) { "height must be positive" }
    }
}