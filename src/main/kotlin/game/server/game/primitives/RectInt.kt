package game.server.game.primitives

import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

data class RectInt(val x0: Int = 0, val y0: Int = 0, val x1: Int = 0, val y1: Int = 0) {

    val width: Int
        get() = x1 - x0 + 1

    val height: Int
        get() = y1 - y0 + 1

    val centerX: Int
        get() = (x0 + x1) / 2

    val centerY: Int
        get() = (y0 + y1) / 2

    val center: PointInt
        get() = PointInt(centerX, centerY)


    init {
        require(x0 <= x1) { "x0 must be <= x1" }
        require(y0 <= y1) { "y0 must be <= y1" }
    }

    fun isInclude(other: RectInt): Boolean =
            other.x0 >= x0 && other.x1 <= x1 && other.y0 >= y0 && other.y1 <= y1

    fun contains(point: PointInt): Boolean =
            point.x >= x0 && point.y >= y0 && point.x <= x1 && point.y <= y1

    fun intersects(other: RectInt) =
            other.x1 >= x0 && other.x0 <= x1 && other.y1 >= y0 && other.y0 <= y1

    fun changeBoundsBy(by: Int = 1): RectInt {
        return RectInt(x0 - by, y0 - by, x1 + by, y1 + by)
    }

    fun intersectsWithEdge(point: PointInt): Boolean {
        return (point.x == x0 || point.x == x1) && (point.y >= y0 && point.y <= y1)
                || (point.y == y0 || point.y == y1) && (point.x >= x0 && point.x <= x1)
    }

    fun minManhattanDistanceToEdge(point: PointInt): Int {
        val x = when {
            point.x > x1 -> point.x - x1
            point.x < x0 -> x0 - point.x
            else -> min(x1 - point.x, point.x - x0)
        }
        val y = when {
            point.y > y1 -> point.y - y1
            point.y < y0 -> y0 - point.y
            else -> min(y1 - point.y, point.y - y0)
        }
        return x + y
    }

    fun chebyshevDistanceTo(that: PointInt): Int {
        val x = when {
            that.x > x1 -> that.x - x1
            that.x < x0 -> x0 - that.x
            else -> 0
        }
        val y = when {
            that.y > y1 -> that.y - y1
            that.y < y0 -> y0 - that.y
            else -> 0
        }
        return max(x, y)
    }

    fun manhattanDistanceTo(that: PointInt): Int {
        val x = when {
            that.x > x1 -> that.x - x1
            that.x < x0 -> x0 - that.x
            else -> 0
        }
        val y = when {
            that.y > y1 -> that.y - y1
            that.y < y0 -> y0 - that.y
            else -> 0
        }
        return x + y
    }

    fun nearPointTo(other: PointInt): PointInt {
        val x = when {
            other.x < x0 -> x0
            other.x > x1 -> x1
            else -> other.x
        }
        val y = when {
            other.y < y0 -> y0
            other.y > y1 -> y1
            else -> other.y
        }
        return PointInt(x, y)
    }

    fun manhattanDistanceToCertainEdge(point: PointInt, dir: PointInt): Int {
        val x = when {
            dir.x < 0 -> abs(point.x - x0)
            dir.x > 0 -> abs(point.x - x1)
            else -> 0
        }
        val y = when {
            dir.y < 0 -> abs(point.y - y0)
            dir.y > 0 -> abs(point.y - y1)
            else -> 0
        }
        return x + y
    }

    companion object {
        fun of(point: PointInt, size: SizeInt) = RectInt(point.x, point.y, point.x + max(size.width - 1, 0), point.y + max(size.height - 1, 0))
    }
}