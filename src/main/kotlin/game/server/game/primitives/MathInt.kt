package game.server.game.primitives

object MathInt {
    fun rectOf(edge: PointInt, size: SizeInt): RectInt = RectInt(edge.x, edge.y, edge.x + size.width, edge.y + size.height)

    fun rectOfPointCenter(center: PointInt, size: SizeInt): RectInt = RectInt(
            center.x - size.width / 2,
            center.y - size.height / 2,
            center.x + size.width / 2,
            center.y + size.height / 2
    )


    /**
     * \ 0 /
     *  \ /
     *3  X  1
     *  / \
     * / 2 \
     *
     * rotation -> [0, 360)
     *
     * (315, 45] -> 0
     * (45, 135] -> 1
     * (135, 225] -> 2
     * (225, 315] -> 3
     *
     *
     * return [0..3]
     * */
    fun getRotationIndex(rotation: Int): Int {
        val angle = when (rotation) {
            in 0 until 360 -> rotation
            else -> if (rotation > 360) (rotation % 360) else (rotation % 360 + 360)
        }
        return ((angle + 45) / 90) % 4
    }

    fun rotate(rect: RectInt, rotation: Int): RectInt {
        val index = getRotationIndex(rotation)
        when (index) {
            0, 2 -> return rect
            else/*1, 3*/ -> return RectInt(
                    rect.x0,
                    rect.y0,
                    rect.x0 + rect.y1 - rect.y0,
                    rect.y0 + rect.x1 - rect.x0
            )
        }
    }

    fun rotate(rect: RectInt, point: PointInt, rotation: Int): RectInt {
        val index = getRotationIndex(rotation)
        when (index) {
            0 -> return rect
            1 -> return RectInt(
                    point.x - (rect.y1 - point.y),
                    point.y - (rect.x1 - point.x),
                    point.x - (rect.y0 - point.y),
                    point.y - (rect.x0 - point.x)
            )
            2 -> return RectInt(
                    point.x - (rect.x1 - point.x),
                    point.y - (rect.y1 - point.y),
                    point.x - (rect.x0 - point.x),
                    point.y - (rect.y0 - point.y)
            )
            else/*3*/ -> return RectInt(
                    point.x + (rect.y0 - point.y),
                    point.y + (rect.x0 - point.x),
                    point.x + (rect.y1 - point.y),
                    point.y + (rect.x1 - point.x)
            )
        }
    }

    fun magnitude(currentPosition: PointInt, targetPoint: PointInt): Int {
        val x = targetPoint.x - currentPosition.x
        val y = targetPoint.y - currentPosition.y
        return Math.sqrt((x * x + y * y).toDouble()).toInt()
    }

}

operator fun PointInt.times(a: SizeInt): PointInt = PointInt(x * a.width, y * a.height)
operator fun SizeInt.times(a: Int): SizeInt = SizeInt(width * a, height * a)
