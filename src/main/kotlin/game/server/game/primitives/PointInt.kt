package game.server.game.primitives

import kotlin.math.*

data class PointInt(val x: Int = 0, val y: Int = 0) : Comparable<PointInt> {
    override fun compareTo(other: PointInt): Int {
        if (x == other.x) return y.compareTo(other.y)
        return x.compareTo(other.x)
    }

    fun isLeftOfLine(from: PointInt, to: PointInt): Boolean {
        return crossProduct(from, to) > 0
    }

    fun crossProduct(origin: PointInt, p2: PointInt): Int {
        return (p2.x - origin.x) * (this.y - origin.y) - (p2.y - origin.y) * (this.x - origin.x)
    }

    fun distanceToLine(a: PointInt, b: PointInt): Double {
        return Math.abs((b.x - a.x) * (a.y - this.y) - (a.x - this.x) * (b.y - a.y)) /
                Math.sqrt(Math.pow((b.x - a.x).toDouble(), 2.0) + Math.pow((b.y - a.y).toDouble(), 2.0))
    }

    fun euclideanDistanceTo(that: PointInt): Double {
        val dx = that.x - x
        val dy = that.y - y
        return sqrt((dx * dx + dy * dy).toDouble())
    }

    fun manhattanDistanceTo(that: PointInt): Int {
        return abs(that.x - x) + abs(that.y - y)
    }

    fun heuristicDistance(that: PointInt): Int = heuristicDistance(this, that)

    fun directionTo(that: PointInt): PointInt = directionTo(this, that)

    companion object {

        val ZERO: PointInt = PointInt(0, 0)

        // < 0 : Counterclockwise
        // = 0 : p, q and r are colinear
        // > 0 : Clockwise
        fun orientation(p: PointInt, q: PointInt, r: PointInt): Int {
            return (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y)
        }

        fun heuristicDistance(p: PointInt, q: PointInt): Int {
            val dx = Math.abs(p.x - q.x)
            val dy = Math.abs(p.y - q.y)
            return max(dx, dy) * 2 + min(dx, dy)
        }

        fun directionTo(from: PointInt, to: PointInt): PointInt {
            return PointInt((to.x - from.x).sign, (to.y - from.y).sign)
        }
    }

    operator fun plus(a: PointInt): PointInt = PointInt(x + a.x, y + a.y)
    operator fun times(a: Int): PointInt = PointInt(x * a, y * a)
    operator fun div(a: Int): PointInt = PointInt(x / a, y / a)

    operator fun minus(other: PointInt): PointInt = PointInt(x - other.x, y - other.y)

    /**
     * @return [0..360)
     * */
    fun toDegrees(): Int {
        return ((atan2(y.toDouble(), x.toDouble()) + PI) * 180 / PI).toInt() % 360
    }
}