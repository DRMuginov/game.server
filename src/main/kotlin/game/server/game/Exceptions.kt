package game.server.game

class GameException(message: String? = null, cause: Throwable? = null): RuntimeException(message, cause)