package game.server.game.api.data

import com.fasterxml.jackson.annotation.JsonClassDescription
import com.fasterxml.jackson.annotation.JsonPropertyDescription
import com.fasterxml.jackson.databind.JsonNode
import game.server.game.BonusData


data class GetAccountsRequest(
        override val offset: Int = 0,
        override val count: Int = 0,
        @get:JsonPropertyDescription("Фильтр по имени (ищется подстрока без учёта регистра с поддержкой * и ?)")
        val name: String? = null,
        @get:JsonPropertyDescription("Фильтр по блокировке")
        val blocked: Boolean? = null,
        @get:JsonPropertyDescription("Фильтр по уровню")
        val level: IntRangeRequest? = null,
        @get:JsonPropertyDescription("Сортировка")
        val sort: SortDataAccountOrderField? = null
): IRequestOffsetCount

data class SortDataAccountOrderField(
        override val orders: List<SortOrderDataAccountOrderField>?
) : SortData<SortOrderDataAccountOrderField, AccountOrderField>

data class SortOrderDataAccountOrderField(
        override val field: AccountOrderField,
        override val dir: OrderDirection = OrderDirection.ASC
) : SortOrderData<AccountOrderField>

data class GetAccountsResponse(
        override val items: List<AccountAdminShortInfo>,
        override val total: Int
): IResponseOffsetCount<AccountAdminShortInfo>

data class AccountAdminShortInfo(
        override val accountId: Long,
        val name: String,
        val level: Int,
        val banned: Boolean
) : AccountId

data class AccountAddRequest(
        override val accountId: Long,
        val count: Long
) : AccountId

data class GetAccountInfoRequest(
        override val accountId: Long
) : AccountId

data class GetAccountInfoResponse(
        override val accountId: Long,
        val name: String,
        val level: Int,
        val exp: Long,
        val points: Long,
        val bonuses: List<BonusData>
) : AccountId

@JsonClassDescription("Поля для сортировки")
enum class AccountOrderField {
    @JsonPropertyDescription("Идентификатор аккаунта")
    account_id,
    @JsonPropertyDescription("Имя")
    account_name,
    @JsonPropertyDescription("Уровень")
    level,
    @JsonPropertyDescription("Заблокирован")
    blocked
}

interface AccountId {
    @get:JsonPropertyDescription("Идентификатор аккаунта")
    val accountId: Long
}

data class AccountDeleteRequest(
        override val accountId: Long
) : AccountId