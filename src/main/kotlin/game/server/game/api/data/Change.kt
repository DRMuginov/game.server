package game.server.game.api.data

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.KeyDeserializer
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.common.collect.ArrayListMultimap
import game.server.config.BaseConfig
import game.server.game.primitives.PointInt
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties

interface IFieldsHolder

abstract class FieldsHolder<B : FieldsHolder<B>> : IFieldsHolder {

    val values: List<Field<B, *>> by lazy { values() }
    private val nameMap: Map<String, Field<B, *>> by lazy { values.associateBy { it.name } }

    @Suppress("UNCHECKED_CAST")
    private fun values(): List<Field<B, *>> {
        return this::class.memberProperties
                .map { it as KProperty1<B, Any> }
                .map { it.get(this as B) }
                .filter { it is Field<*, *> }
                .map { it as Field<B, *> }
    }

    fun valueOf(name: String): Field<B, *>? {
        return nameMap[name]
    }

    internal inline fun <reified T> B.field(name: String): Field<B, T> {
        return field(name, T::class.java)
    }

    @Suppress("UNCHECKED_CAST")
    internal fun <T> field(name: String, type: Class<T>): Field<B, T> {
        return Field(name, type, this as B)
    }
}

@JsonSerialize(using = FieldSerializer::class)
@JsonDeserialize(using = FieldDeserializer::class)
class Field<out F : IFieldsHolder, T>(val name: String, val type: Class<T>, val fields: F) {
    override fun toString() = "$name <${type.simpleName}>"
}

abstract class IFieldValue<out F : IFieldsHolder, T>(
        val field: Field<F, T>
) {
    abstract val value: T

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is IFieldValue<*, *>) return false

        if (field != other.field) return false
        if (value != other.value) return false

        return true
    }

    override fun hashCode(): Int {
        var result = field.hashCode()
        result = 31 * result + (value?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "${javaClass.simpleName}(field=$field, value=$value)"
    }


}

@JsonSerialize(using = FieldValueSerializer::class)
@JsonDeserialize(using = FieldValueDeserializer::class)
class FieldValue<out F : IFieldsHolder, T>(field: Field<F, T>, override val value: T) : IFieldValue<F, T>(field)

class FieldValueBool<out F : IFieldsHolder>(field: Field<F, Boolean>, val boolValue: Boolean) : IFieldValue<F, Boolean>(field) {
    override val value: Boolean
        get() = boolValue
}

class FieldValueLong<out F : IFieldsHolder>(field: Field<F, Long>, val longValue: Long) : IFieldValue<F, Long>(field) {
    override val value: Long
        get() = longValue
}

class FieldValueInt<out F : IFieldsHolder>(field: Field<F, Int>, val intValue: Int) : IFieldValue<F, Int>(field) {
    override val value: Int
        get() = intValue
}

@Suppress("UNCHECKED_CAST", "NOTHING_TO_INLINE")
inline fun <F : IFieldsHolder, T> IFieldValue<F, *>.valueAs(@Suppress("UNUSED_PARAMETER") field: Field<F, T>): T {
    return value as T
}

@Suppress("UNCHECKED_CAST", "NOTHING_TO_INLINE")
inline fun <F : IFieldsHolder> IFieldValue<F, *>.valueAs(@Suppress("UNUSED_PARAMETER") field: Field<F, Boolean>): Boolean {
    if (this is FieldValueBool<F>) return this.boolValue
    return value as Boolean
}

@Suppress("UNCHECKED_CAST", "NOTHING_TO_INLINE")
inline fun <F : IFieldsHolder> IFieldValue<F, *>.valueAs(@Suppress("UNUSED_PARAMETER") field: Field<F, Int>): Int {
    if (this is FieldValueInt<F>) return this.intValue
    return value as Int
}

@Suppress("UNCHECKED_CAST", "NOTHING_TO_INLINE")
inline fun <F : IFieldsHolder> IFieldValue<F, Long>.valueAs(@Suppress("UNUSED_PARAMETER") field: Field<F, Long>): Long {
    if (this is FieldValueLong<F>) return this.longValue
    return value as Long
}

class FieldKeyDeserializer : KeyDeserializer() {
    override fun deserializeKey(key: String, ctxt: DeserializationContext): Any {
        return AccountFields.valueOf(key)!!
    }
}

class FieldKeySerializer : StdSerializer<Field<*, *>>(Field::class.java) {
    override fun serialize(value: Field<*, *>, gen: JsonGenerator, provider: SerializerProvider) {
        gen.writeFieldName(value.name)
    }

}

class FieldSerializer : StdSerializer<Field<*, *>>(Field::class.java) {
    override fun serialize(value: Field<*, *>, gen: JsonGenerator, provider: SerializerProvider) {
        gen.writeString(value.name)
    }

}

class FieldDeserializer : StdDeserializer<Field<IFieldsHolder, *>>(Field::class.java) {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Field<IFieldsHolder, *>? {
        return AccountFields.valueOf(p.text)
    }
}

class FieldValueSerializer : StdSerializer<FieldValue<*, *>>(FieldValue::class.java) {
    override fun serialize(value: FieldValue<*, *>, gen: JsonGenerator, provider: SerializerProvider) {
        gen.writeStartObject()
        gen.writeFieldName(value.field.name)
        gen.writeObject(value.value)
        gen.writeEndObject()
    }
}

@Suppress("UNCHECKED_CAST")
class FieldValueDeserializer : StdDeserializer<FieldValue<*, *>>(FieldValue::class.java) {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): FieldValue<*, *>? {
        p.nextFieldName()
        val name = p.text
        val field = AccountFields.valueOf(name) as Field<IFieldsHolder, Any>
        p.nextValue()
        val value = p.readValueAs(field.type)
        p.nextToken()
        return FieldValue(field, value)
    }
}

object AccountFields : FieldsHolder<AccountFields>() {
    val EXP = field<Long>("exp")
    val LEVEL = field<Int>("level")
    val NEXT_LEVEL_EXP = field<Int>("nextLevelExp")
    val FACTION = field<String>("faction")
}

object GameObjectFields : FieldsHolder<GameObjectFields>() {
    val EXP = field<Long>("exp")
    val POS = field<PointInt>("pos")
    val ANGLE = field<Int>("angle")
    val TRIGGER = field<String>("trigger")
}

data class Update(
        val fields: MutableList<FieldValue<*, *>> = mutableListOf()
)


@JsonSerialize(keyUsing = FieldKeySerializer::class)
@JsonDeserialize(keyUsing = FieldKeyDeserializer::class)
class UpdateMap<in F : IFieldsHolder> {

    private val map = ArrayListMultimap.create<Field<F, *>, Any?>()
//    private val map = HashMap<Field<F, *>, Any?>()


    operator fun <T> set(key: Field<F, T>, value: T) {
        map.put(key, value)
    }

    fun <T> put(key: Field<F, T>, value: T) = set(key, value)

    fun <T> put(fieldValue: IFieldValue<F, T>) {
        set(fieldValue.field, fieldValue.value)
    }

    @Suppress("UNCHECKED_CAST")
    operator fun <T> get(key: Field<F, T>): List<T> {
        return map[key] as List<T>
    }

}


fun main(args: Array<String>) {
    val jsonMapper = BaseConfig().jsonMapper()
    val data = Update()
    data.fields.add(FieldValue(AccountFields.EXP, 1))
    data.fields.add(FieldValue(AccountFields.NEXT_LEVEL_EXP, 2))
    data.fields.add(FieldValue(AccountFields.FACTION, "dd"))
    val writeValueAsString = jsonMapper.writeValueAsString(data)
    println(writeValueAsString)
    val readValue = jsonMapper.readValue<Update>(writeValueAsString)
    println(readValue)

    val updateMap = UpdateMap<IFieldsHolder>()
    updateMap[AccountFields.EXP] = 1
    updateMap[AccountFields.FACTION] = "f"

    val writeValueAsString2 = jsonMapper.writeValueAsString(updateMap)
    println(writeValueAsString2)
}