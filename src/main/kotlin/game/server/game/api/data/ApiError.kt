package game.server.game.api.data

enum class ApiError(val code: Int) {
    NOT_FOUND(1),
    BAD_LOGIN_OR_PASSWORD(2),
    NOT_AUTHORIZED(3),
    AUTHORIZED(4),
    ACCOUNT_NOT_FOUND(5),
    PLAYER_NOT_ONLINE(6),
    GAME_NOT_FOUND(7),
    PLAYERS_NOT_FOUND(8),
    CONNECTION_NOT_DETECTED(9),
    METHOD_NOT_FOUND(10)
}