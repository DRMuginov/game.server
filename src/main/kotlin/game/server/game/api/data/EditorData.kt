package game.server.game.api.data

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.NullNode

data class SaveGenericData(
        var idType: String = "",
        var data: JsonNode = NullNode.instance
)

data class GetGenericData(
        var idType: String = "",
        var id: String = ""
)

data class GetAllGenericData(
        var idType: String = ""
)

data class PlaceData(
        var typeNetPlace: String = "",
        var placeSizeY: Int = 0,
        var placeSizeX: Int = 0,
        var placeID: Long = 0,
        var listInfoOom: Array<ListInfoOomItem> = arrayOf()
)

data class OomPos(
        var x: Int = 0,
        var z: Int = 0
)

data class ListInfoOomItem(
        var oomID: String = "",
        var oomRotate: Int = 0,
        var oomPos: OomPos = OomPos()
)

data class EditorAuthToken(
        val token: String
) {
    object example {
        fun example() = EditorAuthToken("550e8400-e29b-41d4-a716-446655440000")
    }
}

data class EditorAuthLoginReg(
        val login: String,
        val password: String,
        val roles: List<String>
) {
    companion object : IAccountId {
        override val accountId: Long
            get() = 234

        fun example() = EditorAuthLoginReg("test2", "megapassword", listOf("admin"))
    }
}

data class EditorAuthLogin(
        val login: String,
        val password: String
) {
    companion object {
        fun exampla() = EditorAuthLogin("test", "passwordhash")
    }
}