package game.server.game.api.data

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.databind.JsonNode
import game.server.game.*


interface IAccountId {
    @get:JsonPropertyDescription("Идентификатор аккаунта")
    val accountId: Long
}

data class GameRoomId(
    @get: JsonPropertyDescription("Идентификатор игровой комнаты")
    val gameRoomId: Long
)

@JsonClassDescription("Краткая информация об аккаунте")
data class LoginData(
        override val accountId: Long = 0
) : IAccountId

data class AccountData(
        override val accountId: Long = 0,
        @field:JsonPropertyDescription("имя (логин, идентификатор игрового сервиса и т.д.)")
        val name: String,
        @field:JsonPropertyDescription("Текущий уровень")
        val level: Int,
        @field:JsonPropertyDescription("Опыт")
        val exp: Long,
        @field:JsonPropertyDescription("Опыта до следующего уровня")
        val nextLevelExp: Long,
        @field:JsonPropertyDescription("Очки")
        var points: Long = 0L,
        @field:JsonPropertyDescription("Номер игровой комнаты, если игрок участвует в матче")
        var gameRoomId: Long? = null
) : IAccountId

data class AccountUpdateData(
        override val accountId: Long
) : IAccountId {
    var exp: Long? = null
    var expAdded: List<Long>? = null
    var nextLevelExp: Long? = null
    var level: Int? = null
    var points: Long? = null
    var gameRoomId: Long? = null
    var bonuses: MutableList<BonusUpdateData>? = null
    var sessions: List<ListChangeData<AccountSessionData, Unit>>? = null
}

data class AccountSessionData(val peerInfo: String)

data class GetOnlineAccountsData(
        val players: List<AccountData> = emptyList()
)

@JsonPropertyOrder("type")
interface IType {
    @get:JsonPropertyDescription("Тип класса")
    val type: String
}


data class AddData(
        val count: Long
)

data class AddBonusesData(
        @field:JsonPropertyDescription("Список бонусов")
        val resources: List<BonusCount> = emptyList()
)

data class SendData(
        val message: JsonNode
)

data class TimerData(
        val total: Long,
        val passed: Long,
        val enabled: Boolean
)

data class TimerUpdateData(
        var total: Long? = null,
        var passed: Long? = null,
        var enabled: Boolean? = null
)

interface IRequestOffsetCount {
    @get:JsonPropertyDescription("Индекс первого элемента")
    val offset: Int
    @get:JsonPropertyDescription("Количество элементов")
    val count: Int
}

interface IResponseOffsetCount<T> {
    @get:JsonPropertyDescription("Список запрошенных элементов")
    val items: List<T>
    @get:JsonPropertyDescription("Общее количество элементов")
    val total: Int
}

@JsonClassDescription("Направление сортировки")
enum class OrderDirection {
    @JsonPropertyDescription("По возрастанию")
    ASC,
    @JsonPropertyDescription("По убыванию")
    DESC,
}

@JsonClassDescription("Сортировка")
interface SortData<O : SortOrderData<T>, T : Enum<T>> {
    val orders: List<O>?
}

@JsonClassDescription("Свойство и направление для сортировки")
interface SortOrderData<T : Enum<T>> {
    @get: JsonPropertyDescription("Свойство, по которому идёт сортировка")
    val field: T
    val dir: OrderDirection
}

@JsonClassDescription("Диапазон")
data class IntRangeRequest(
        @get:JsonPropertyDescription("Минимальное значение (включительно)")
        val from: Int? = null,
        @get:JsonPropertyDescription("Максимальное значение (включительно)")
        val to: Int? = null
)
