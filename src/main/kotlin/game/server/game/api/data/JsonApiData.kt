package game.server.game.api.data

import com.fasterxml.jackson.databind.JsonNode

data class JsonMessage(
        val type: JsonMessageType = JsonMessageType.EVENT,
        val name: String? = null,
        val id: String? = null,
        val data: JsonNode? = null
)

data class JsonError(
        val message: String = "",
        val details: String? = null
)

enum class JsonMessageType {
    METHOD,
    RESULT,
    EVENT,
    ERROR,
}