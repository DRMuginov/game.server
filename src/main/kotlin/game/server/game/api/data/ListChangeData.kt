package game.server.game.api.data

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonTypeName


@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type",
        visible = false
)
@JsonSubTypes(
        JsonSubTypes.Type(value = ListChangeData.Add::class),
        JsonSubTypes.Type(value = ListChangeData.Remove::class),
        JsonSubTypes.Type(value = ListChangeData.Update::class),
        JsonSubTypes.Type(value = ListChangeData.Clear::class)
)
interface ListChangeData<T, U> : IType {

    @JsonTypeName(ListChanges.ADD)
    data class Add<T, U>(
            val index: Int = -1,
            val item: T
    ) : ListChangeData<T, U> {
        override val type: String = ListChanges.ADD

        companion object {
            fun example(): List<Add<String, Unit>> = listOf(
                    Add(0, "item1"),
                    Add(1, "item2")
            )
        }
    }

    @JsonTypeName(ListChanges.REMOVE)
    data class Remove<T, U>(
            val index: Int
    ) : ListChangeData<T, U> {
        override val type: String = ListChanges.REMOVE
    }

    @JsonTypeName(ListChanges.UPDATE)
    data class Update<T, U>(
            val index: Int,
            val change: U
    ) : ListChangeData<T, U> {
        override val type: String = ListChanges.UPDATE

        companion object {
            fun example(): List<Update<String, String>> = listOf(
                    Update(0, "item12"),
                    Update(1, "item13")
            )
        }
    }

    @JsonTypeName(ListChanges.CLEAR)
    class Clear<T, U> : ListChangeData<T, U> {
        override val type: String = ListChanges.CLEAR
    }
}

object ListChanges {
    const val ADD = "add"
    const val REMOVE = "remove"
    const val UPDATE = "update"
    const val CLEAR = "clear"
}