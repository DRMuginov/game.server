package game.server.game.api

import com.fasterxml.jackson.annotation.JsonPropertyDescription
import game.server.game.api.data.*
import game.server.game.api.utils.ApiEvent
import kotlin.reflect.full.memberProperties

object GameApiEvents {
    @JsonPropertyDescription("Отладочный лог")
    val DEBUG = create<String>("event.debug")
    @JsonPropertyDescription("Уведомление о успешной авторизации")
    val LOGIN = create<LoginData>("event.login")
    @JsonPropertyDescription("Аккаунт забанен")
    val BANNED = create<Unit>("event.banned")
    @JsonPropertyDescription("Изменена информация об аккаунте")
    val UPDATE_ACCOUNT = create<AccountUpdateData>("event.update_account")
    @JsonPropertyDescription("Изменена информация об аккаунте")
    val START_GAME = create<GameRoomId>("event.start_game")
    @JsonPropertyDescription("Изменена информация об аккаунте")
    val SEND_DATA = create<SendData>("event.send_data")
    @JsonPropertyDescription("Изменена информация об аккаунте")
    val END_GAME = create<GameRoomId>("event.end_game")


    inline fun <reified T> create(name: String): ApiEvent<T> {
        return ApiEvent(name, T::class.java)
    }

    fun values(): Array<ApiEvent<*>> {
        return GameApiEvents::class.memberProperties
                .map { it.get(GameApiEvents) }
                .map { it as ApiEvent<*> }
                .toTypedArray()
    }
}