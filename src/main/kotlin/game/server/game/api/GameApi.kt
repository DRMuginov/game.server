package game.server.game.api

import com.fasterxml.jackson.annotation.JsonPropertyDescription
import com.fasterxml.jackson.databind.JsonNode
import game.server.game.api.data.*
import game.server.game.api.utils.ApiMethod
import game.server.services.AuthLogin
import kotlin.reflect.full.memberProperties

object GameApi {

    @JsonPropertyDescription("Регистрация нового аккаунта")
    val AUTH_REGISTER = create<AuthLogin, Unit>("auth.register")

    @JsonPropertyDescription("Авторизация пользователя")
    val AUTH_LOGIN = create<AuthLogin, Unit>("auth.login")

    @JsonPropertyDescription("Получение данных профиля")
    val PLAYER_GET_INFO = create<Unit, AccountData>("player.get_info")

    @JsonPropertyDescription("Добавить очков")
    val PLAYER_ADD_POINTS = create<AddData, Unit>("player.add_points")

    @JsonPropertyDescription("Добавить опыт")
    val PLAYER_ADD_EXP = create<AddData, Unit>("player.add_exp")

    @JsonPropertyDescription("Добавить бонусы")
    val PLAYER_ADD_BONUSES = create<AddBonusesData, Unit>("player.add_bonuses")

    @JsonPropertyDescription("Отправляет список онлайн игроков")
    val GAME_GET_PLAYERS = create<Unit, GetOnlineAccountsData>("game.get_players")

    @JsonPropertyDescription("Запрос на начало игры")
    val GAME_START_GAME = create<AccountId, Unit>("game.start_game")

    @JsonPropertyDescription("Переправка данных другому игроку")
    val GAME_SEND_DATA = create<JsonNode, Unit>("game.send_data")

    @JsonPropertyDescription("Запрос на  окончание игры")
    val GAME_END_GAME = create<GameRoomId, Unit>("game.end_game")

    val GAME_GET_FILE = create<GameRoomId, Unit>("game.get_file")

    inline fun <reified PARAMS, reified RESULT> create(name: String): ApiMethod<PARAMS, RESULT> {
        return ApiMethod(name, PARAMS::class.java, RESULT::class.java)
    }

    fun values(): Array<ApiMethod<*, *>> {
        return GameApi::class.memberProperties
                .map { it.get(GameApi) }
                .map { it as ApiMethod<*, *> }
                .toTypedArray()
    }

}

enum class GameApiTag {
    @JsonPropertyDescription("Разное")
    DEFAULT,
    @JsonPropertyDescription("Авторизация / Регистрация")
    AUTH,
    @JsonPropertyDescription("Игровые")
    GAME,
    @JsonPropertyDescription("Для разработки")
    CHEAT,
    @JsonPropertyDescription("Утилитарные")
    UTIL,
}