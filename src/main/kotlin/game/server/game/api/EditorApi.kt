package game.server.game.api

import com.fasterxml.jackson.annotation.JsonPropertyDescription
import com.fasterxml.jackson.databind.JsonNode
import game.server.game.api.data.*
import game.server.game.api.utils.ApiMethod
import kotlin.reflect.full.memberProperties

object EditorApi {
    const val BASE_PATH = "api/editor/"
    const val AUTH_LOGIN_REG_NAME = "editor.auth_login_reg"
    const val AUTH_LOGIN_NAME = "editor.auth_login"

    @JsonPropertyDescription("Войти под существующим пользователем")
    val AUTH_LOGIN = create<EditorAuthLogin, EditorAuthToken>(AUTH_LOGIN_NAME)
    @JsonPropertyDescription("Создать нового пользователя")
    val AUTH_LOGIN_REG = create<EditorAuthLoginReg, EditorAuthToken>(AUTH_LOGIN_REG_NAME)

    @JsonPropertyDescription("Пинг")
    val PING = create<Unit, Unit>("editor.ping")

//    val SAVE_GENERIC = create<SaveGenericData, Unit>("editor.save_generic")
//    val GET_GENERIC = create<GetGenericData, JsonNode>("editor.get_generic")
//    val GET_ALL_GENERIC = create<GetAllGenericData, List<JsonNode>>("editor.get_all_generic")

    @JsonPropertyDescription("Получить информацию об аккаунте")
    val GET_ACCOUNT_INFO = create<GetAccountInfoRequest, GetAccountInfoResponse>("editor.get_account_info")
    @JsonPropertyDescription("Получить список аккаунтов")
    val GET_ACCOUNTS = create<GetAccountsRequest, GetAccountsResponse>("editor.get_accounts")
    @JsonPropertyDescription("Удалить аккаунт")
    val ACCOUNT_DELETE = create<AccountDeleteRequest, Unit>("editor.account_delete")
    @JsonPropertyDescription("Изменить опыта")
    val ACCOUNT_ADD_EXP = create<AccountAddRequest, Unit>("editor.account_add_exp")
    @JsonPropertyDescription("Изменить очки")
    val ACCOUNT_ADD_POINTS = create<AccountAddRequest, Unit>("editor.account_add_points")


    inline fun <reified PARAMS, reified RESULT> create(name: String): ApiMethod<PARAMS, RESULT> {
        return ApiMethod(name, PARAMS::class.java, RESULT::class.java)
    }

    fun values(): Array<ApiMethod<*, *>> {
        return EditorApi::class.memberProperties
                .filter { !it.isConst }
                .map { it.get(EditorApi) }
                .filter { it is ApiMethod<*, *> }
                .map { it as ApiMethod<*, *> }
                .toTypedArray()
    }
}