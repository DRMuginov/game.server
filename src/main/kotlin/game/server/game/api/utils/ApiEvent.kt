package game.server.game.api.utils

data class ApiEvent<T>(
        val name: String,
        val type: Class<T>
) {
    fun create(data: T): ApiEventData<T> {
        return ApiEventData(this, data)
    }
}