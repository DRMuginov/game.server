package game.server.game.api.utils

import reactor.core.publisher.Mono

interface ApiChannel {
    fun <T, R> call(apiMethod: ApiMethod<T, R>, params: T): Mono<R>
}