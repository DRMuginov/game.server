package game.server.game.api.utils

data class ApiMethod<PARAMS, RESULT>(
        val name: String,
        val paramsType: Class<PARAMS>,
        val resultType: Class<RESULT>
)