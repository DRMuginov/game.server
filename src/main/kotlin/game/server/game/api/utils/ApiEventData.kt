package game.server.game.api.utils

import game.server.game.GameEvent

data class ApiEventData<T>(
        val apiEvent: ApiEvent<T>,
        val data: T
): GameEvent