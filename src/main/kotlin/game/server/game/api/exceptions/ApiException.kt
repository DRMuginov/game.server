package game.server.game.api.exceptions

import game.server.game.api.data.ApiError

class ApiException (
        error: ApiError,
        details: String? = null
): RuntimeException(if (details==null) error.name else "${error.name}. $details")