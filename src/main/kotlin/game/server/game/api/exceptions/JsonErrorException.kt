package game.server.game.api.exceptions

import game.server.game.api.data.JsonError

class JsonErrorException(
        val jsonError: JsonError
) : RuntimeException(jsonError.message + (if (jsonError.details == null) "" else ". ${jsonError.details}"))