package game.server.game.util

class ObservableListOnly<T>(
        list: MutableList<T> = ArrayList()
) : ObservableListBase<T>(list)