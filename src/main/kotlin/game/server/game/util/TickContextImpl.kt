package game.server.game.util

class TickContextImpl(
        override var deltaTime: Int = 0,
        override var frame: Long = 0,
        override var time: Long = 0
) : TickContext