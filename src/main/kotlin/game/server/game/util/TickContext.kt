package game.server.game.util

interface TickContext {
    /**
     * In milliseconds
     */
    val deltaTime: Int
    val frame: Long
    val time: Long
}