package game.server.game.util

import game.server.game.api.data.*
import game.server.utils.Action1
import game.server.utils.plus

interface IObservable<T> {
    var observer: Action1<T>?
}

interface UpdateSupport<T> {
    fun sendUpdate(change: T)
}

interface FieldsUpdateSupport<T : IFieldsHolder> : UpdateSupport<IFieldValue<T, *>>

abstract class AbstractUpdateObservable<T> : IObservable<T>, UpdateSupport<T> {
    override var observer: Action1<T>? = null

    override fun sendUpdate(change: T) {
        observer?.invoke(change)
    }
}

abstract class AbstractFieldsUpdateObservable<T : IFieldsHolder> : AbstractUpdateObservable<IFieldValue<T, *>>()

@Suppress("NOTHING_TO_INLINE")
inline fun <OBSERVABLE : IObservable<T>, T> OBSERVABLE.addObserver(noinline action: Action1<T>): OBSERVABLE {
    this.observer += action
    return this
}

@Suppress("NOTHING_TO_INLINE")
inline fun <T> IObservable<T>.sendUpdate(change: T) {
    observer?.invoke(change)
}

@Suppress("NOTHING_TO_INLINE")
inline fun <F : IFieldsHolder, V> UpdateSupport<IFieldValue<F, *>>.sendUpdate(field: Field<F, V>, value: V) {
    sendUpdate(FieldValue(field, value))
}

@Suppress("NOTHING_TO_INLINE")
inline fun <F : IFieldsHolder> UpdateSupport<IFieldValue<F, *>>.sendUpdate(field: Field<F, Int>, value: Int) {
    sendUpdate(FieldValueInt(field, value))
}

@Suppress("NOTHING_TO_INLINE")
inline fun <F : IFieldsHolder> UpdateSupport<IFieldValue<F, *>>.sendUpdate(field: Field<F, Long>, value: Long) {
    sendUpdate(FieldValueLong(field, value))
}

@Suppress("NOTHING_TO_INLINE")
inline fun <F : IFieldsHolder> UpdateSupport<IFieldValue<F, *>>.sendUpdate(field: Field<F, Boolean>, value: Boolean) {
    sendUpdate(FieldValueBool(field, value))
}