package game.server.game.util

interface ICollectionEvent<T>
interface ICollectionItemEvent<T> : ICollectionEvent<T> {
    val item: T
}

interface ICollectionEventClear<T> : ICollectionEvent<T> {
    val items: List<T>
}

interface ICollectionItemEventAdd<T> : ICollectionItemEvent<T>
interface ICollectionItemEventRemove<T> : ICollectionItemEvent<T>
interface ICollectionItemEventUpdate<T, U> : ICollectionItemEvent<T> {
    val update: U
}

interface IListEvent<T> : ICollectionItemEvent<T> {
    val index: Int
}

interface IListItemEventAdd<T> : IListEvent<T>, ICollectionItemEventAdd<T>
interface IListItemEventRemove<T> : IListEvent<T>, ICollectionItemEventRemove<T>
interface IListItemEventUpdate<T, U> : IListEvent<T>, ICollectionItemEventUpdate<T, U>

data class CollectionEventClear<T>(override val items: List<T>) : ICollectionEventClear<T>
data class CollectionEventItemAdd<T>(override val item: T) : ICollectionItemEventAdd<T>
data class CollectionEventItemRemove<T>(override val item: T) : ICollectionItemEventRemove<T>
data class CollectionEventUpdate<T : IObservable<U>, U>(override val item: T, override val update: U) : ICollectionItemEventUpdate<T, U>

data class ListEventItemAdd<T>(override val item: T, override val index: Int) : IListItemEventAdd<T>
data class ListEventItemRemove<T>(override val item: T, override val index: Int) : IListItemEventRemove<T>
data class ListEventUpdate<T : IObservable<U>, U>(override val item: T, override val index: Int, override val update: U) : IListItemEventUpdate<T, U>


@Suppress("NOTHING_TO_INLINE", "UNCHECKED_CAST")
inline fun <T : IObservable<U>, U> ICollectionEvent<T>.toTypedUpdate(): ICollectionItemEventUpdate<T, U> {
    return this as ICollectionItemEventUpdate<T, U>
}