package game.server.game.util

import game.server.utils.Action1
import game.server.utils.minus
import game.server.utils.plus

class ObservableList<T : IObservable<U>, U>(
        list: MutableList<T> = ArrayList(),
        private val indexedUpdate: Boolean = true
) : ObservableListBase<T>(list) {

    private val subscriptions = ArrayList<Subscribe<U>>()

    private fun onItemUpdate(item: T, update: U) {
        if (indexedUpdate) {
            observer?.invoke(ListEventUpdate(item, indexOf(item), update))
        } else {
            observer?.invoke(CollectionEventUpdate(item, update))
        }
    }

    override fun onAdd(index: Int, item: T) {
        super.onAdd(index, item)
        val observer: Action1<U> = { onItemUpdate(item, it) }
        item.observer += observer
        subscriptions.add(Subscribe(observer))
    }

    override fun onRemove(index: Int, item: T) {
        super.onRemove(index, item)
        val subscribe = subscriptions.removeAt(index)
        item.observer -= subscribe.observer
    }

    override fun onClear(removedItems: List<T>) {
        super.onClear(removedItems)
        (0..removedItems.lastIndex).forEach { i ->
            removedItems[i].observer -= subscriptions[i].observer
        }
        subscriptions.clear()
    }

    data class Subscribe<U>(val observer: Action1<U>)

}