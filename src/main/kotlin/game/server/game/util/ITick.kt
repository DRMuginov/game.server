package game.server.game.util

interface ITick {
    fun tick(tick: TickContext)
}