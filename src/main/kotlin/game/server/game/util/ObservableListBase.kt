package game.server.game.util

import game.server.utils.Action1

abstract class ObservableListBase<T>(
        val list: MutableList<T> = ArrayList()
) : MutableList<T>, IObservable<ICollectionEvent<T>> {
    override var observer: Action1<ICollectionEvent<T>>? = null

    override val size: Int
        get() = list.size

    override fun contains(element: T): Boolean = list.contains(element)

    override fun containsAll(elements: Collection<T>): Boolean = list.containsAll(elements)

    override fun get(index: Int): T = list.get(index)

    override fun indexOf(element: T): Int = list.indexOf(element)

    override fun isEmpty(): Boolean = list.isEmpty()

    override fun iterator(): MutableIterator<T> = list.iterator()

    override fun lastIndexOf(element: T): Int = list.lastIndexOf(element)

    override fun add(element: T): Boolean {
        val add = list.add(element)
        onAdd(list.lastIndex, element)
        return add
    }

    override fun add(index: Int, element: T) {
        list.add(index, element)
        onAdd(index, element)
    }

    override fun addAll(index: Int, elements: Collection<T>): Boolean {
        elements.forEachIndexed { i, item ->
            add(index + i, item)
        }
        return true
    }

    override fun addAll(elements: Collection<T>): Boolean {
        return addAll(list.size, elements)
    }

    override fun clear() {
        val oldItems = list.toList()
        list.clear()
        onClear(oldItems)
    }

    override fun listIterator(): MutableListIterator<T> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun listIterator(index: Int): MutableListIterator<T> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun remove(element: T): Boolean {
        val index = list.indexOf(element)
        if (index != -1) {
            removeAt(index)
            return true
        }
        return false
    }

    override fun removeAll(elements: Collection<T>): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun removeAt(index: Int): T {
        val item = list.removeAt(index)
        onRemove(index, item)
        return item
    }

    override fun retainAll(elements: Collection<T>): Boolean = list.retainAll(elements)

    override fun set(index: Int, element: T): T {
        val oldValue = list.set(index, element)
        onRemove(index, oldValue)
        onAdd(index, element)
        return oldValue
    }

    override fun subList(fromIndex: Int, toIndex: Int): ObservableListBase<T> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    protected open fun onAdd(index: Int, item: T) {
        observer?.invoke(ListEventItemAdd(item, list.lastIndex))
    }

    protected open fun onClear(removedItems: List<T>) {
        observer?.invoke(CollectionEventClear(removedItems))
    }

    protected open fun onRemove(index: Int, item: T) {
        observer?.invoke(ListEventItemRemove(item, index))
    }

}