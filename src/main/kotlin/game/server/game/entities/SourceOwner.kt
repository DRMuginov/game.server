package game.server.game.entities

data class SourceOwner(val source: Any, val parent: SourceOwner? = null)

inline fun <reified T> SourceOwner.findOwnerByType(): T? {
    var o = this
    while (true) {
        val source = o.source
        if (source is T) return source
        val parent = o.parent ?: break
        o = parent
    }
    return null
}