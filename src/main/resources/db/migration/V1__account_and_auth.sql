CREATE TABLE public.account
(
  id       BIGINT PRIMARY KEY                        NOT NULL,
  created  TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  modified TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  json     JSONB                                     NOT NULL
);

CREATE SEQUENCE account_seq
  START WITH 100;


CREATE TABLE public.account_auth
(
  id         BIGINT PRIMARY KEY,
  account_id BIGINT,
  created    TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  modified   TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
  auth_type  TEXT                                      NOT NULL,
  json       JSONB                                     NOT NULL
);

CREATE UNIQUE INDEX account_auth_login_index
  ON public.account_auth (auth_type, (json ->> 'login'))
  WHERE auth_type = 'login';
CREATE UNIQUE INDEX account_auth_gpg_id_index
  ON public.account_auth (auth_type, (json ->> 'playerId'))
  WHERE auth_type = 'gpg';
CREATE UNIQUE INDEX account_auth_game_center_player_id_index
  ON public.account_auth (auth_type, (json ->> 'playerId'))
  WHERE auth_type = 'game_center';

ALTER TABLE account_auth
  ADD CONSTRAINT account_auth_check_auth_type
CHECK (auth_type IN ('login', 'gpg', 'game_center') );

CREATE SEQUENCE account_auth_seq
  START WITH 100;

ALTER TABLE public.account_auth
  ALTER COLUMN id SET DEFAULT nextval('account_auth_seq' :: REGCLASS);