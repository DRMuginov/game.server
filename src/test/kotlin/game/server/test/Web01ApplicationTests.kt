package game.server.test

import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.context.WebApplicationContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.setup.MockMvcBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@SpringBootTest
@RunWith(SpringRunner::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class Web01ApplicationTests {
	private val baseUrl = "http://localhost:8080/products/"
	private val jsonContentType = MediaType(MediaType.APPLICATION_JSON.type, MediaType.APPLICATION_JSON.subtype)
    private lateinit var mockMvc: MockMvc

	@Autowired
	private lateinit var webAppContext: WebApplicationContext

	@Before
	fun before() {
		mockMvc = webAppContextSetup(webAppContext).build()
	}

	/*@Test
	fun `1 - Get empty list of products`() {
		val request = get(baseUrl).contentType(jsonContentType)

		mockMvc.perform(request)
				.andExpect(status().isOk)
				.andExpect(content().json("[]", true))
	}*/

	@Test
	fun `2 - Add first user`() {
		val passedJsonString = """
            {
                "login": "SainTTetriS",
                "password": "Qwerty12345"
            }
        """.trimIndent()

		val request = post(baseUrl).contentType(jsonContentType).content(passedJsonString)

		val resultJsonString = """
            {
                "login": "SainTTetriS",
                "password": "Qwerty12345",
                "id": 1
            }
        """.trimIndent()

		mockMvc.perform(request)
				.andExpect(status().isCreated)
				.andExpect(content().json(resultJsonString, true))
	}

	/*@Test
	fun `3 - Update first product`() {
		val passedJsonString = """
            {
                "name": "iPhone 4S",
                "description": "Smart phone by Apple"
            }
        """.trimIndent()

		val request = put(baseUrl + "1").contentType(jsonContentType).content(passedJsonString)

		val resultJsonString = """
            {
                "name": "iPhone 4S",
                "description": "Smart phone by Apple",
                "id": 1
            }
        """.trimIndent()

		mockMvc.perform(request)
				.andExpect(status().isOk)
				.andExpect(content().json(resultJsonString, true))
	}

	@Test
	fun `4 - Get first product`() {
		val request = get(baseUrl + "1").contentType(jsonContentType)

		val resultJsonString = """
            {
                "name": "iPhone 4S",
                "description": "Smart phone by Apple",
                "id": 1
            }
        """.trimIndent()

		mockMvc.perform(request)
				.andExpect(status().isFound)
				.andExpect(content().json(resultJsonString, true))
	}

	@Test
	fun `5 - Get list of products, with one product`() {
		val request = get(baseUrl).contentType(jsonContentType)

		val resultJsonString = """
            [
                {
                    "name": "iPhone 4S",
                    "description": "Smart phone by Apple",
                    "id": 1
                }
            ]
        """.trimIndent()

		mockMvc.perform(request)
				.andExpect(status().isOk)
				.andExpect(content().json(resultJsonString, true))
	}*/

	/*@Test
	fun `6 - Delete first product`() {
		val request = delete(baseUrl + "4").contentType(jsonContentType)

		mockMvc.perform(request).andExpect(status().isOk)

        val request1 = delete(baseUrl + "5").contentType(jsonContentType)

        mockMvc.perform(request1).andExpect(status().isOk)

        val request2 = delete(baseUrl + "6").contentType(jsonContentType)

        mockMvc.perform(request2).andExpect(status().isOk)
	}*/

}
